//
//  TransactionRModel.swift
//  conecte
//
//  Created by Adonio Silva on 31/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class TransactionPresenter {
    
    
    let model: TransactionRModel.Item
    
    init (_ model: TransactionRModel.Item) {
        self.model = model
    }
    
    
    lazy var createdAt = Carbon.parseFromJson(model.created_at)!
    
    lazy var operationTypeDesc: String = {
        switch model.operation_type {
            case "fees": return "taxas"
            case "credit": return "crédito"
            default: return model.operation_type
        }
    }()
    
    lazy var operationTypeCl: UIColor = {
        switch model.operation_type {
        case "fees": return Cl.mc.orange500Color()
        case "credit": return Cl.mc.green500Color()
        default: return Cl.mc.blue500Color()
        }
    }()
    
}
