//
//  UserFullPresenter.swift
//  conecte
//
//  Created by Adonio Silva on 11/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class UserFullPresenter {
    
    let model: UserFullRModel
    
    init (_ model: UserFullRModel) {
        self.model = model
    }
    
    //
    
    class func isClassDateValid(_ needle: Date, haystack: [(String,String)]) -> Bool {
        for hor in haystack {
            let dayofweek = hor.0
            let turno = hor.1
            
            if Carbon(needle).inBrRange(dayOfWeek: dayofweek, shift: turno) { return true }
        }
        
        return false
    }
    
    
    //
    lazy var firstLetterName = String(model.name.trimmed.uppercased().prefix(1))
    
    lazy var skills = model.skills.map { SkillItem.f($0) }
    
    lazy var horariosDesc: String = skills
        .filter { $0.header.starts(with: "Horários") }
        .map { $0.middle + " - " + $0.footer }
        .joined(separator: ", ")
        .def("-sem horarios-")
    
    lazy var horariosList = skills
        .filter { $0.header.starts(with: "Horários") }
        .map { ($0.middle, $0.footer) }
    
    lazy var priceDesc: String? = skills
        .filter({ $0.header.starts(with: "Hora Aula") })
        .first?.parent.pivot.comp
    
    lazy var skillDesc: String = skills
        .filter { $0.header.starts(with: "Habilid") }
        .map { $0.middle }
        .uniqued()
        .joined(separator: ", ")
        .def("-sem habilidades-")
    
    lazy var skillList: [(String,String)] = self.skills
        .filter { $0.header.starts(with: "Habilida") }
        .map { ($0.middle, $0.footer) }
    
    //
    struct SkillItem {
        
        var parent: UserFullRModel.Skill
        var header: String
        var middle: String
        var footer: String
        
        static func f(_ model: UserFullRModel.Skill) -> SkillItem {
            let sp = model.desc.split(separator: "/").map { String($0) }
            if (sp.count <= 1) {
                return SkillItem(parent: model, header: "Habilidade", middle: "0", footer: sp.first ?? "-")
            }
            if (sp.count == 2) {
                return SkillItem(parent: model, header: "Habilidade", middle: sp[0], footer: sp[1])
            }
            return SkillItem(parent: model, header: sp[0], middle: sp[1], footer: sp.last!)
        }
    }
    
}
