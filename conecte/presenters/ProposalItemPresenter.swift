//
//  ProposalItemPresenter.swift
//  conecte
//
//  Created by Adonio Silva on 16/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class ProposalItemPresenter {
    
    let model: ProposalItemRModel
    
    init (_ model: ProposalItemRModel) {
        self.model = model
    }
    
    lazy var expireAt = Carbon.parseFromJson(model.expire_at)!
    lazy var createdAt = Carbon.parseFromJson(model.created_at)!
    
    lazy var isPhasePending = self.model.phase < 3 && self.expireAt.isFuture()
    lazy var isPhaseDone = self.model.phase == 3
    lazy var isPhaseExpired = self.model.phase < 3 && !self.expireAt.isFuture()
    
    
    
}
