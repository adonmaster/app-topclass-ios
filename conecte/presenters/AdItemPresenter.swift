//
//  AdItemPresenter.swift
//  conecte
//
//  Created by Adonio Silva on 29/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class AdItemPresenter {
    
    let model: AdRModel.Item
    
    init (_ model: AdRModel.Item) {
        self.model = model
    }
    
    
    lazy var createdAt = Carbon.parseFromJson(model.created_at)!
    
    func isUserClient(userId: Int) -> Bool {
        return model.client_user_id == userId
    }
    
    func isUserRelated(userId: Int) -> Bool {
        return isUserClient(userId: userId) || isUserSomePro(userId: userId)
    }
    
    func isUserSomePro(userId: Int) -> Bool {
        return findProFrom(userId: userId) != nil
    }
    
    private func findProFrom(userId: Int) -> AdRModel.Pr? {
        return model.pros.first { $0.id == userId }
    }
    
    func findPriceProFrom(userId: Int, def: Double) -> Double {
        return findProFrom(userId: userId)?.price ?? def
    }
    
}
