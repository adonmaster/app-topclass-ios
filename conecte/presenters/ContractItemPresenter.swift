//
//  ContractItemRModel.swift
//  conecte
//
//  Created by Adonio Silva on 17/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class ContractItemPresenter {
    
    let model: ContractRModel.Item
    
    init (_ model: ContractRModel.Item) {
        self.model = model
    }
    
    lazy var dueAt = Carbon.parseFromJson(model.due_at)!
    lazy var createdAt = Carbon.parseFromJson(model.created_at)!
    
}
