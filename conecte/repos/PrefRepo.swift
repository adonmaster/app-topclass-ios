//
//  PrefRepo.swift
//  conecte
//
//  Created by Adonio Silva on 26/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import CoreData

class PrefRepo {
    
    lazy var context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func getModel(key: String) -> ConfigModel?
    {
        let request: NSFetchRequest<ConfigModel> = ConfigModel.fetchRequest()
        request.predicate = NSPredicate(format: "key = %@", key)
        request.fetchLimit = 1
        return try? context.fetch(request).first
    }
    
    func setModel(key: String, str: String?, int: Int?, float: Double?)
    {
        let model = getModel(key: key) ?? ConfigModel(context: context)
        model.key = key
        model.value_flo = float ?? 0
        model.value_int = Int16(int ?? 0)
        model.value_str = str
        try! context.save()
    }
    
    func removeModel(key: String) {
        if let model = getModel(key: key) {
            context.delete(model)
            try! context.save()
        }
    }
    
    func clearAll() {
        try! context.execute(NSBatchDeleteRequest(fetchRequest: ConfigModel.fetchRequest()))
    }
    
    func set(_ key: String, _ val: Bool) {
        setModel(key: key, str: nil, int: val ? 1 : 0, float: 0)
    }
    
    func get(_ key: String, def: Bool=false) -> Bool {
        if let model = getModel(key: key) {
            return model.value_int > 0
        }
        return def
    }
    
    func get(_ key: String, def: Int=0) -> Int? {
        if let v = getModel(key: key)?.value_int {
            return Int(v)
        }
        return def
    }
    
    func set(_ key: String, _ val: Int?) {
        if let v = val {
            setModel(key: key, str: nil, int: v, float: nil)
        } else {
            remove(key)
        }
    }
    
    func get(_ key: String, _ def: String) -> String {
        return getModel(key: key)?.value_str ?? def
    }
    
    func get(_ key: String) -> String? {
        return getModel(key: key)?.value_str
    }
    
    func set(_ key: String, _ val: String?) {
        if let v = val {
            setModel(key: key, str: v, int: nil, float: nil)
        } else {
            remove(key)
        }
    }
    
    func get(_ key: String, def: Double) -> Double {
        return getModel(key: key)?.value_flo ?? def
    }
    
    func set(_ key: String, _ val: Double) {
        setModel(key: key, str: nil, int: nil, float: val)
    }
    
    func remove(_ key: String) {
        removeModel(key: key)
    }
    
    func first(_ key: String, cb: ()->Void) {
        let k = "ProfRepo@first_\(key)"
        if !get(k) {
            set(k, true)
            cb()
        }
    }
    
    func firstView(_ v: UIView, key: String, cb: ()->Void) {
        v.isHidden = true
        first(key) {
            v.isHidden = false
            cb()
        }
    }
    
    // ****
    
    func setLastStoryboard(_ s: String) {
        set("PrefRepo@lastStoryboard", s)
    }
    
    func getLastStoryboard() -> String? {
        return get("PrefRepo@lastStoryboard")
    }
    
    
    // token
    
    func getDeviceToken() -> String? {
        return get("PrefRepo@deviceToken")
    }
    
    func setDeviceToken(v: String)
    {
        let oldToken: String! = getDeviceToken()
        
        if oldToken == nil || oldToken != v
        {
            set("PrefRepo@deviceToken", v)
            
            setDeviceTokenSent(false)
        }
    }
    
    func wasDeviceTokenSent() -> Bool {
        return get("PrefRepo@isDeviceTokenSent")
    }
    
    func setDeviceTokenSent(_ option: Bool) {
        set("PrefRepo@isDeviceTokenSent", option)
    }
    
    
    
}
