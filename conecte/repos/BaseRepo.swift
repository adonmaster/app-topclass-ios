//
//  BaseRepo.swift
//  conecte
//
//  Created by Adonio Silva on 26/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import CoreData

class BaseRepo<T: NSManagedObject> {
    
    var context: NSManagedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    let EV_ON_ERROR = "BaseRepo@onError"
    
    func _first(condition: String?=nil, args: [Any]?=nil, sort: [NSSortDescriptor]?=nil) -> T?
    {
        return _get(limit: 1, condition: condition, args: args, sort: sort).first
    }
    
    func _get(limit: Int?=nil, condition: String?=nil, args: [Any]?=nil, sort: [NSSortDescriptor]?=nil) -> [T]
    {
        let request = buildRequest(condition, args, sort)
        if let l = limit {
            request.fetchLimit = l
        }
        
        return (try? context.fetch(request)) ?? []
    }

    func _count(condition: String?=nil, args: [Any]?=nil) -> Int
    {
        let request = buildRequest(condition, args, nil)
        return (try? context.count(for: request)) ?? 0
    }
    
    func _exists(condition: String?=nil, args: [Any]?=nil) -> Bool
    {
        return _count(condition: condition, args: args) > 0
    }
    
    func _delete(_ item: T)
    {
        context.delete(item)
        
        try? context.save()
    }
    
    func _delete(where condition: String?=nil, args: [Any]?=nil)
    {
        let classString = String(describing: T.self)
        
        let fetch = NSFetchRequest<NSFetchRequestResult>(entityName: classString);
        if let c = condition {
            fetch.predicate = NSPredicate(format: c, argumentArray: args)
        }
        
        let r = NSBatchDeleteRequest(fetchRequest: fetch);
        try! context.execute(r);
    }
    
    func _deleteAll() {
        _delete()
    }
    
    private func buildRequest(_ condition: String?, _ args: [Any]?, _ sort: [NSSortDescriptor]?) -> NSFetchRequest<T>
    {
        let request: NSFetchRequest<T> = T.fetchRequest() as! NSFetchRequest<T>
        if let c = condition {
            request.predicate = NSPredicate(format: c, argumentArray: args)
        }
        request.sortDescriptors = sort
        return request;
    }
    
    func _buildQuery(condition: String, args: [Any], q: String?, fieldsToQuery: [String], sort: [NSSortDescriptor]) -> [T]
    {
        let qry = q ?? ""
        let list = qry
            .components(separatedBy: CharacterSet(charactersIn: " .-;,"))
            .map({ trim($0) })
            .compactMap({ $0.count > 0 ? $0 : nil })
        
        var innerSql: [[String]] = []
        var innerParams: [Any] = []
        for qStr in list {
            innerSql.append(fieldsToQuery.map({ "\($0) CONTAINS[cd] %@" }))
            fieldsToQuery.forEach({ _ in innerParams.append(qStr) })
        }
        
        let sqlAddon: String = innerSql
            .map({ $0.map({ "(\($0))" }).joined(separator: " or ") })
            .map({ "(\($0))" }).joined(separator: " and ")
        
        let sql = condition + (sqlAddon.count > 0 ? " AND (\(sqlAddon))" : "")
        
        return self._get(condition: sql, args: args + innerParams, sort: sort)
    }
    

    private func trim(_ s: String) -> String {
        return s.trimmed
    }
    
}
