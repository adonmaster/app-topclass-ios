//
//  UserRepo.swift
//  conecte
//
//  Created by Adonio Silva on 26/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import CoreData

class UserRepo: BaseRepo<User> {
    
    func save(remote_id: Int, name: String, email: String, avatar: String?, token: String, isPro: Bool) -> User
    {
        let model = _first(condition: "email = %@", args: [email]) ?? User(context: context)
        
        model.email = email
        model.remote_id = Int16(remote_id)
        model.name = name
        model.avatar = avatar
        model.token = token
        model.is_pro = isPro
        
        try? context.save()
        
        return model
    }
    
    func activateUser(email: String)
    {
        let users = _get()

        users.forEach { u in u.is_active = u.email == email }
        
        try? context.save()
    }
    
    func firstActive() -> User? {
        return _first(condition: "is_active = %@", args: [true], sort: nil)
    }
    
    func updateUser(email: String, name: String, avatar: String?) -> User {
        let model = _first(condition: "email = %@", args: [email])!
        
        model.name = name
        model.avatar = avatar
        
        try? context.save()
        
        return model
    }
}
