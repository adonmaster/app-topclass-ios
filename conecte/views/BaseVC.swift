//
//  BaseVC.swift
//  conecte
//
//  Created by Adonio Silva on 09/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class BaseVC: UIViewController {
    
    @IBAction func onLeftMenu(_ sender: Any) {
        openLeft()
    }
    
    class func scrollViewDidScroll(
        parentView: UIView, scrollView: UIScrollView, uiHeaderWrapper: UIView, uiHeaderTopCon: NSLayoutConstraint,
        forceHiding: Bool = false
    )
    {
        // animate header
        let scrollh = scrollView.frame.height
        let conth = scrollView.contentSize.height
        let wraph = uiHeaderWrapper.frame.height
        if (conth - wraph > scrollh || forceHiding) {
            let v = ceil(scrollView.contentOffset.y) <= 0
                ? 0 : -wraph
            uiHeaderTopCon.constant = v
            UIView.animate(withDuration: 0.3) {
                parentView.layoutIfNeeded()
            }
        }
        
    }
    
}
