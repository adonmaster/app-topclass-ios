//
//  ProposalDetailExVC.swift
//  conecte
//
//  Created by Adonio Silva on 23/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import MapKit

class ProposalDetailVC: UIViewController {
    
    //
    @IBOutlet weak var uiId: UILabel?
    
    @IBOutlet weak var uiProAvatar: ImageViewRounded!
    @IBOutlet weak var uiProTitle: UILabel!
    @IBOutlet weak var uiProDesc: UILabel!
    
    @IBOutlet weak var uiCliAvatar: ImageViewRounded!
    @IBOutlet weak var uiCliName: UILabel!
    @IBOutlet weak var uiCliDesc: UILabel!
    
    @IBOutlet weak var uiAulaTitle: UILabel!
    @IBOutlet weak var uiAula: UILabel!
    
    @IBOutlet weak var uiMap: MKMapView!
    @IBOutlet weak var uiLocation: UILabel!
    
    @IBOutlet weak var uiPrice: UILabel!
    
    @IBOutlet weak var uiInfoWrapper: UIStackView!
    @IBOutlet weak var uiInfoIcon: UILabel!
    @IBOutlet weak var uiInfoDesc: UILabel!
    
    @IBOutlet weak var uiBtnAccept: ButtonEx!
    @IBOutlet weak var uiBtnDelete: UIBarButtonItem!
    
    
    //
    private var mModel: ProposalItemRModel!
    
    //
    func setData(_ model: ProposalItemRModel) {
        mModel = model
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        var idText = [
            "Proposta #\(mModel.id)",
            "Criada \(mModel.p.createdAt.format(pattern: "dd/MM/yyyy HH:mm"))",
            "--"
        ]
        mModel.body
            .items
            .enumerated()
            .forEach { tu in
                idText.append(
                    "Aula #\(tu.offset + 1) - \(tu.element.dtObject.format(pattern: "dd/MM/yyyy HH:mm"))"
                )
            }
        uiId?.text = idText.joined(separator: "\n")

        //
        ImgUtil.load(url: mModel.pro_user.avatar_thumb, uiProAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        uiProTitle.text = mModel.pro_user.name
        uiProDesc.text = mModel.pro_user.email
        
        //
        ImgUtil.load(url: mModel.client_user.avatar_thumb, uiCliAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        uiCliName.text = mModel.client_user.name
        uiCliDesc.text = mModel.client_user.email
        
        //
        uiAulaTitle.text = Str.plural(mModel.body.items.count, "", "3. Aula", { "3. Aulas (\($0))" })
        
        //
        let aulaText = Span("")
        mModel.body.items.enumerated().forEach { (i: Int, item: ProposalItemRModel.BodyV2Item) in
            if aulaText.count > 0 { aulaText.append("\n\n") }
            
            let dt = Carbon.parseFromJson(item.dt)!
            let dtHuman = dt.diffForHuman()
            let duration = item.duration
            let materia = item.materia
            aulaText.append("\(i+1). ").boldSystem(size: 15.0).append("\(dt) (\(dtHuman))")
                .append("\n\(materia)")
                .append("\nDuração: \(duration) mins").color(Cl.mc.grey500Color())
        }
        uiAula.attributedText = aulaText.get()

        //
        uiLocation.text = [mModel.body.address_line, mModel.body.address_comp]
            .compactMap { $0 }.joined(separator: "\n")
        
        //
        uiMap.delegate = self
        setupMap(
            CLLocationCoordinate2D(latitude: mModel.body.address_lat, longitude: mModel.body.address_lng),
            mModel.client_user.name
        )
        
        //
        uiPrice.text = "R$ " + Str.number(mModel.price)
        
        //
        let expCar = Carbon.parseFromJson(mModel.expire_at)!
        let expiration = expCar.format(pattern: "dd 'de' MMMM") + " (\(expCar.diffForHuman()))"
        var obs: String? = nil
        switch mModel.phase {
        case 1: obs = "Aguardando professor aceitar...\nProposta expira \(expiration)"
        case 2: obs = "Aguardando aluno contratar aula...\nProposta expira \(expiration)"
        case 3:
            uiInfoIcon.text = ""
            obs = "Contrato já feito"
        default:
            obs = nil
        }
        uiInfoWrapper.isVisible = obs != nil
        uiInfoDesc.text = obs
        
        //
        let appUserRemoteId = Session.shared.user!.remote_id
        uiBtnAccept.isVisible = appUserRemoteId != mModel.pro_user.id
            && mModel.phase == 1
        
        navigationItem.rightBarButtonItems =
            mModel.client_user.id == appUserRemoteId && mModel.phase == 1
            ? [uiBtnDelete] : []
    }
    
    private func setupMap(_ coord: CLLocationCoordinate2D, _ title: String) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coord
        annotation.title = title

        let region = MKCoordinateRegion(center: coord, latitudinalMeters: 1200, longitudinalMeters: 1200)
        
        uiMap.setRegion(region, animated: true)
        uiMap.addAnnotation(annotation)
    }

    @IBAction func onAccept(_ sender: Any) {
        Dialog(sender: self)
            .ok().cancel()
            .title("Proposta")
            .msg("Tem certeza que deseja aceitar esta proposta?")
            .present(onOk: {
                
                self.reallyAccept()
                
            }, onCancel: nil, onDestructive: nil, nil)
    }
    
    struct FormAccept: Encodable {
        var id: Int
    }
    private func reallyAccept() {
        let msg = "Aceitando proposta..."
        if msg == Notify.cLoading { return }
        
        Notify.cLoading = msg
        Http.shared.post(BaseRModel.self, uri: "proposals/accept", FormAccept(id: mModel.id))
            .then { [weak self] _ in
                
                self?.performSegue(withIdentifier: "sg_to_done", sender: nil)
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done {
                Notify.cLoading = nil
            }
    }
    
    
    @IBAction func onTryDestroy(_ sender: Any) {
        Dialog(sender: self)
            .destructive("Apagar").cancel()
            .title("Proposta")
            .msg("Deseja mesmo remover este registro?")
            .present(onOk: nil, onCancel: nil, onDestructive: {
                self.destroy()
            }, nil)
    }
    
    struct FormDelete: Encodable {}
    private func destroy() {
        let msg = "Removendo proposta..."
        if Notify.cLoading == msg { return }
        
        Notify.cLoading = msg
        Http.shared.postDelete(BaseRModel.self, uri: "proposal/\(mModel.id)", FormDelete())
            .then { [weak self] _ in
                
                self?.navigationController?.popViewController(animated: true)
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done {
                Notify.cLoading = nil
            }
    }
    
}

// MARK:  map
extension ProposalDetailVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let ann = view.annotation {
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: ann.coordinate))
            mapItem.name = ann.title ?? "Conecte Aulas"
            mapItem.openInMaps(launchOptions: [:])
        }
    }
    
}
