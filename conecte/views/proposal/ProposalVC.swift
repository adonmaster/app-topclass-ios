//
//  ProposalVC.swift
//  conecte
//
//  Created by Adonio Silva on 16/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ProposalVC: BaseVC {
    
    //
    @IBOutlet var uiHeaderWrapper: UIView!
    @IBOutlet var uiHeaderTopCon: NSLayoutConstraint!
    @IBOutlet var uiTv: UITableView!
    @IBOutlet var uiSegment: UISegmentedControl!
    @IBOutlet var uiEmpty: UIView!
    
    //
    private var loading = false
    private var mModel: ProposalRModel!
    private var mData = [ProposalItemRModel]()
    private var tvLast: ProposalItemRModel!
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        uiTv.tableFooterView = UIView()
        uiTv.initRefresher(caller: self, action: #selector(onRefresh(_:)))
        
        //
        uiSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Cl.mc.whiteColor()], for: .normal)
        uiSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Cl.mc.blackColor()], for: .selected)
    }
    
    @objc func onRefresh(_ sender: Any) {
        updateData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateData()
    }
    
    
    @IBAction func onSegment(_ sender: Any) {
        updateDesktop()
    }
    
    private func updateData() {
        // halt
        if Session.shared.user == nil || loading {
            return
        }
        
        loading = true
        Notify.cError = nil
        uiTv.isRefreshAnimating = true
        
        Http.shared.get(ProposalRModel.self, uri: "proposals", params: [:])
            .then { [weak self] model in
                
                self?.mModel = model
                self?.updateDesktop()
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }.done { [weak self] in
                self?.loading = false
                self?.uiTv.isRefreshAnimating = false
            }
    }
    
    private func updateDesktop()
    {
        mData = mModel.payload.filter({ item in
            switch uiSegment.selectedSegmentIndex {
            case 0: return item.p.isPhasePending
            case 1: return item.p.isPhaseDone
            case 2: return item.p.isPhaseExpired
            default: return false
            }
        })
        
        if uiSegment.selectedSegmentIndex != 0 {
            mData = mData.reversed()
        }
        
        //
        [
            ("Pendente", mModel.payload.filter({ $0.p.isPhasePending }).count),
            ("Aceito", mModel.payload.filter({ $0.p.isPhaseDone }).count),
            ("Expirado", mModel.payload.filter({ $0.p.isPhaseExpired }).count)
        ].enumerated().forEach { index, tuple in
            var title = tuple.0
            if tuple.1 > 0 { title = title + " (\(tuple.1))" }
            uiSegment.setTitle(title, forSegmentAt: index)
        }
        
        //
        uiTv.reloadData()
    }
    
    @IBAction func unProposal(_ segue: UIStoryboardSegue) {
        // do nothing :)
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ProposalDetailVC {
            vc.setData(tvLast)
        }
    }
    
}

// MARK: table view
extension ProposalVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let n = mData.count
        uiEmpty.isVisible = n == 0
        return n
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ProposalCell
        let m = mData[indexPath.row]
        
        cell.uiId.text = "Proposta #\(m.id) - \(m.p.createdAt.formatBrDate())"
        
        ImgUtil.load(url: m.pro_user.avatar_thumb, cell.uiProAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        cell.uiProName.text = m.pro_user.name
        
        ImgUtil.load(url: m.client_user.avatar_thumb, cell.uiCliAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        cell.uiCliName.text = m.client_user.name

        cell.uiLocation.text = m.body.address_line
        
        cell.uiPrice.text = "R$ " + Str.number(m.price)
        
        return cell
    }

    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if uiSegment.selectedSegmentIndex < 2 {
            tvLast = mData[indexPath.row]
            performSegue(withIdentifier: "sgToDetail", sender: nil)
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        BaseVC.scrollViewDidScroll(
            parentView: view, scrollView: scrollView, uiHeaderWrapper: uiHeaderWrapper,
            uiHeaderTopCon: uiHeaderTopCon, forceHiding: true
        )
    }
    
}
