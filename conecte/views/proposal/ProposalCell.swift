//
//  ProposalCell.swift
//  conecte
//
//  Created by Adonio Silva on 16/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ProposalCell: UITableViewCell {
    
    @IBOutlet weak var uiId: UILabel!
    @IBOutlet weak var uiProAvatar: ImageViewRounded!
    @IBOutlet weak var uiProName: UILabel!
    @IBOutlet weak var uiCliAvatar: ImageViewRounded!
    @IBOutlet weak var uiCliName: UILabel!
    @IBOutlet weak var uiLocation: UILabel!
    @IBOutlet weak var uiPrice: UILabel!

}
