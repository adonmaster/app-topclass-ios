//
//  CardEditDialog.swift
//  conecte
//
//  Created by Adonio Silva on 13/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class CardEditDialogVC: UIViewController {
    
    //
    @IBOutlet weak var uiCardNo: UITextField!
    @IBOutlet weak var uiCardValid: UITextField!
    @IBOutlet weak var uiCardCvv: UITextField!
    @IBOutlet weak var uiCardName: UITextField!
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiCardNo.text = Repo.pref.get("card@no")
        uiCardValid.text = Repo.pref.get("card@valid")
        uiCardCvv.text = Repo.pref.get("card@cvv")
        uiCardName.text = Repo.pref.get("card@name")
    }
    
    private func validate() -> String?
    {
        if Str.count(uiCardNo.text) < (4 * 4) + 3 { return "Número do cartão requerido" }
        
        if Str.count(uiCardValid.text) < 2 + 1 + 4 { return "Data de validade requerido" }
        let validPack = uiCardValid.text!.split(separator: "/").map { String($0) }
        
        let validMonth = Int(validPack[0]) ?? 0
        if validMonth < 1 || validMonth > 12 { return "Mês de validade inválido" }
        
        let validYear = Int(validPack[1]) ?? 0
        if validYear < 2000 { return "Ano de validade inválido" }

        if Str.count(uiCardName.text?.trimmed) < 5 { return "Nome requerido" }
        
        return nil
    }
    
    
    @IBAction func onSubmit(_ sender: Any)
    {
        let err = validate()
        Notify.cError = err
        if err != nil { return }
        
        //
        Repo.pref.set("card@no", uiCardNo.text)
        Repo.pref.set("card@valid", uiCardValid.text)
        Repo.pref.set("card@cvv", uiCardCvv.text)
        Repo.pref.set("card@name", uiCardName.text!.trimmed)
        
        //
        navigationController?.popViewController(animated: true)
    }
    
}
