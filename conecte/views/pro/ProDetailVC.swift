//
//  ProDetailVC.swift
//  conecte
//
//  Created by Adonio Silva on 08/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ProDetailVC: UIViewController {
    
    //
    @IBOutlet weak var uiAvatar: ImageViewRounded!
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiDesc1: UILabel!
    @IBOutlet weak var uiDesc2: UILabel!
    @IBOutlet weak var uiSegment: UISegmentedControl!
    @IBOutlet weak var uiTv: UITableView!
    @IBOutlet weak var uiEmpty: UIStackView!
    
    //
    var oModel: UserFullRModel!
    private var mData: [Item] = [
        Item(isHeader: true, desc: "dino"),
        Item(isHeader: false, desc: "dino2"),
        Item(isHeader: false, desc: "dino3"),
        Item(isHeader: false, desc: "dino4"),
        Item(isHeader: true, desc: "dino5"),
        Item(isHeader: false, desc: "dino6")
    ]
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        uiTv.tableFooterView = UIView()
        
        //
        ImgUtil.load(url: oModel.avatar_thumb, uiAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        uiTitle.text = oModel.name
        uiDesc1.text = oModel.email
        uiDesc2.text = oModel.p.priceDesc
        
        //
        updateTable()
    }
    
    @IBAction func onSegment(_ sender: Any) {
        updateTable()
    }
    
    private func updateTable()
    {
        // filtering skills
        let skills = oModel.p.skills.filter { s in
            var startKey: String
            switch uiSegment.selectedSegmentIndex {
                case 0: startKey = "Habilid"
                case 1: startKey = "Horários"
                default: startKey = "none"
            }
            return s.header.starts(with: startKey)
        }
        
        // preparing tree list
        var list = [(String,[String])]()
        for s in skills {
            if let id: Int = list.firstIndex(where: { $0.0 == s.middle }) {
                list[id].1.append(s.footer)
            } else {
                list.append((s.middle, [s.footer]))
            }
        }
        
        // populating
        mData = []
        for l in list {
            mData.append(Item(isHeader: true, desc: l.0))
            for ii in l.1 {
                mData.append(Item(isHeader: false, desc: ii))
            }
        }
        
        // update ui
        uiTv.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
        // pro proposal vc
        if let ctrl = segue.destination as? UINavigationController,
            let vc = ctrl.topViewController as? ProProposalVC
        {
            vc.oUser = oModel
        }
    }
    
    //
    private struct Item {
        var isHeader: Bool
        var desc: String
    }
}

extension ProDetailVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        uiEmpty.isVisible = mData.count == 0
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let m = mData[indexPath.row]
        let id = m.isHeader ? "cell-header" : "cell-item"
        let cell = tableView.dequeueReusableCell(withIdentifier: id)!
        
        if let c = cell as? ProDetailCellHeader {
            c.uiDesc.text = m.desc
        } else if let c = cell as? ProDetailCellItem {
            c.uiDesc.text = m.desc
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
}
