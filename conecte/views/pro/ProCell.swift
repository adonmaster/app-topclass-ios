//
//  ProCell.swift
//  conecte
//
//  Created by Adonio Silva on 05/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ProCell: UITableViewCell {
    
    @IBOutlet weak var uiAvatar: ImageViewRounded!
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiDesc1: UILabel!
    @IBOutlet weak var uiDesc2: UILabel!
    @IBOutlet weak var uiRight1: UILabel!
    
}
