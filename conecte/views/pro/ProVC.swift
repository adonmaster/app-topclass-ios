//
//  ProVC.swift
//  conecte
//
//  Created by Adonio Silva on 05/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ProVC: BaseVC {

    // ui
    
    @IBOutlet weak var uiTv: UITableView!
    @IBOutlet weak var uiHeaderWrapper: UIView!
    @IBOutlet weak var uiHeaderTopCon: NSLayoutConstraint!
    @IBOutlet weak var uiSearch: UITextField!
    @IBOutlet weak var uiEmpty: UIStackView!
    
    //
    private var mData = [(String, [UserFullRModel])]()
    private var mModels = [UserFullRModel]() {
        didSet {
            mData = Arr(mModels.sorted(by: { $0.p.firstLetterName < $1.p.firstLetterName }))
                .categorize({ $0.p.firstLetterName })
        }
    }
    private var loading = false
    
    // methods
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiTv.tableFooterView = UIView()
        uiTv.initRefresher(caller: self, action: #selector(refresherTv(_:)))
        
        uiSearch.delegate = self
    }
    
    @objc func refresherTv(_ sender: Any) {
        updateData()
    }
    
    private func updateData() {
        // halt
        if Session.shared.user == nil || loading {
            return
        }
        
        loading = true
        Notify.cError = nil
        uiTv.isRefreshAnimating = true
        
        let queryString = uiSearch.text?.trimmed ?? ""
        let params = queryString.count > 0 ? ["q": queryString] : [:]
        
        Http.shared.get(ProQueryRModel.self, uri: "pro/query", params: params)
            .then { [weak self] model in
                
                self?.mModels = model.payload
                self?.uiTv.reloadData()
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done { [weak self] in
                self?.uiTv.isRefreshAnimating = false
                self?.loading = false
            }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ProDetailVC, let m = mm(nil) {
            vc.oModel = m
        }
    }
    
}

// MARK: tableview
extension ProVC: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        uiEmpty?.isVisible = mData.count <= 0
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return mData[section].0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData[section].1.count
    }
    
    private func mm(_ index: IndexPath?) -> UserFullRModel? {
        guard let i = index ?? uiTv.indexPathForSelectedRow else { return nil }
        return mData[i.section].1[i.row]
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ProCell
        guard let m = mm(indexPath) else { return cell }
        
        //
        ImgUtil.load(url: m.avatar_thumb, cell.uiAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        
        //
        cell.uiTitle.text = m.name
        
        //
        cell.uiDesc1.text = m.email
        
        //
        cell.uiDesc2.text = m.p.skillDesc
        
        //
        cell.uiRight1.text = m.p.priceDesc ?? "-"
        
        return cell
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        BaseVC.scrollViewDidScroll(
            parentView: view, scrollView: scrollView, uiHeaderWrapper: uiHeaderWrapper, uiHeaderTopCon: uiHeaderTopCon, forceHiding: true
        )
        
    }
    
}


extension ProVC: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        updateData()
        App.i.hideKeyboard()
        
        return true
    }
    
}
