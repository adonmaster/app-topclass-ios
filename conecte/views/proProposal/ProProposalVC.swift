//
//  ProProposalVC.swift
//  conecte
//
//  Created by Adonio Silva on 09/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit



class ProProposalVC: UIViewController {
    
    //
    @IBOutlet weak var uiAvatar: ImageViewRounded!
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiDesc1: UILabel!
    @IBOutlet weak var uiDesc2: UILabel!
    @IBOutlet weak var uiHeaderWrapper: UIView!
    @IBOutlet weak var uiHeaderTopCon: NSLayoutConstraint!
    
    @IBOutlet weak var uiHorTv: UITableView!
    @IBOutlet weak var uiHorTvHeight: NSLayoutConstraint!
    @IBOutlet weak var uiHorEmpty: UILabel!
    @IBOutlet weak var uiHorInfo: UILabel!
    
    @IBOutlet weak var uiLocEmpty: UILabel!
    @IBOutlet weak var uiLocWrapper: UIView!
    @IBOutlet weak var uiLocComp: UITextField!
    @IBOutlet weak var uiLocInfo: UILabel!
    
    //
    private var mDates = [ProposalMultiDialogVC.Item]()
    private var mLocation: SimpleAddress? = nil
    
    //
    var oUser: UserFullRModel!
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        ImgUtil.load(url: oUser.avatar_thumb, uiAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        uiTitle.text = oUser.name
        uiDesc1.text = oUser.email
        uiDesc2.text = oUser.p.priceDesc
        
        uiLocWrapper.isVisible = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func onDtAdd(_ sender: Any) {
        let vc = UIStoryboard(name: "ProposalMultiDialog", bundle: nil).instantiateInitialViewController()
        if let vc = vc as? ProposalMultiDialogVC {
            vc.delegate = self
            vc.oClasses = oUser.p.skillList.map { $0.0 }.uniqued()
            vc.oHorarios = oUser.p.horariosList
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onLocation(_ sender: Any)
    {
        performSegue(withIdentifier: "sgToLocationPicker", sender: nil)
    }
    
    @IBAction func onSubmit(_ sender: Any)
    {
        let err = validate()
        Notify.cError = err
        if err != nil { return }
        
        //
        performSegue(withIdentifier: "sgToSecond", sender: nil)
    }
    
    private func validate() -> String? {
        if mDates.count <= 0 { return "Insira um horário antes de enviar" }
        if mLocation == nil { return "Escolha a localização da aula antes de enviar" }
        if mLocation!.address.range(of: "goiânia", options: .caseInsensitive) == nil {
            return "Atualmente só aceitamos localizações em Goiânia. Aguarde atualizações!"
        }
        return nil
    }
    
    private func updateDesktopLocation() {
        uiLocEmpty.isVisible = mLocation == nil
        uiLocWrapper.isVisible = mLocation != nil
        if let location = mLocation {
            uiLocInfo.text = location.address
        }
    }
    
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    private func horTvReloadData() {
        uiHorTv.reloadData()
        Task.main(delayInSeconds: 0.2) {
            self.uiHorTvHeight.constant = self.uiHorTv.contentSize.height
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ProProposalSecondVC {
            vc.oSetup(
                oUser.name, proUserId: oUser.id, email: oUser.email, avatar: oUser.avatar,
                price: oUser.p.priceDesc!, dates: mDates, locationAddr: mLocation!.address,
                locationComp: uiLocComp.text, locationCoord: (mLocation!.lat, mLocation!.lng)
            )
        }
        else if let nc = segue.destination as? UINavigationController,
            let vc = nc.topViewController as? LocationPicker
        {
            vc.delegate = self
        }
    }
    
    
    //
    struct SimpleAddress {
        var address: String
        var lat: Double
        var lng: Double
    }
    
}

// MARK: location picker
extension ProProposalVC: LocationPickerDelegate {
    
    func locationPicker(onLocation lat: Double, lng: Double, address: String) {
        mLocation = SimpleAddress(address: address, lat: lat, lng: lng)
        updateDesktopLocation()
    }
    
}


// MARK: scrollview delegate
extension ProProposalVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView)
    {
        BaseVC.scrollViewDidScroll(
            parentView: view, scrollView: scrollView, uiHeaderWrapper: uiHeaderWrapper, uiHeaderTopCon: uiHeaderTopCon
        )
    }
    
}

// MARK: proposalmultidialogdelegate
extension ProProposalVC: ProposalMultiDialogDelegate {

    func proposalMultiDialog(onDone res: ProposalMultiDialogVC.Item) {
        mDates.append(res)
        horTvReloadData()
    }
    
}

// MARK: date table
extension ProProposalVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let n = mDates.count
        //
        uiHorTv.isVisible = n > 0
        uiHorEmpty.isVisible = n == 0
        
        //
        uiHorInfo.text = Str.plural(n, "sem aulas programadas", "apenas uma aula programada", { "\($0) aulas programadas" })
        
        //
        return n
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ProProposalDateCell
        let i = indexPath.row
        let m = mDates[i]
        
        cell.setDelegate(delegate: self, index: i)
        
        cell.uiTitle.text = Carbon(m.dt).format(pattern: "dd/MM/yyyy HH:mm")
            + "\n\(m.materia) - \(m.duration) mins"
        
        return cell
    }
    
}

// MARK: date cell delegate
extension ProProposalVC: ProProposalDateCellDelegate {
    
    func proProposalDateCellClose(index: Int) {
        mDates.remove(at: index)
        horTvReloadData()
    }
    
}


