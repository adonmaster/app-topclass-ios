//
//  ProProposalSecondVC.swift
//  conecte
//
//  Created by Adonio Silva on 13/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ProProposalSecondVC: UIViewController {
    
    //
    @IBOutlet weak var uiAvatar: ImageViewRounded!
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiDesc1: UILabel!
    @IBOutlet weak var uiDesc2: UILabel!
    @IBOutlet weak var uiHeaderWrapper: UIView!
    @IBOutlet weak var uiHeaderTopCon: NSLayoutConstraint!
    
    @IBOutlet weak var uiPrice: UILabel!
    
    @IBOutlet weak var uiCardEmpty: UILabel!
    @IBOutlet weak var uiCardBtn: UIButton!
    @IBOutlet weak var uiCardWrapper: UIView!
    @IBOutlet weak var uiCardNo: UILabel!
    @IBOutlet weak var uiCardValid: UILabel!
    @IBOutlet weak var uiCardCvv: UILabel!
    @IBOutlet weak var uiCardName: UILabel!
    
    
    //
    private var mProUserId = 0
    private var mName: String!
    private var mEmail: String!
    private var mAvatar: String?
    private var mPrice: String!
    private var mTotal = 0.0
    private var mDates = [ProposalMultiDialogVC.Item]()
    private var mLocationAddr: String!
    private var mLocationComp: String? = nil
    private var mLocationCoord: (Double, Double)!
    
    private var cCard: Card? {
        get {
            if let no = Repo.pref.get("card@no"),
                let valid = Repo.pref.get("card@valid"),
                let cvv = Repo.pref.get("card@cvv"),
                let name = Repo.pref.get("card@name")
            {
                return Card(no: no, valid: valid, cvv: cvv, name: name)
            }
            return nil
        }
    }
    
    
    //
    
    func oSetup(
        _ name: String, proUserId: Int, email: String, avatar: String?, price: String,
        dates: [ProposalMultiDialogVC.Item], locationAddr: String, locationComp: String?,
        locationCoord: (Double, Double)
    )
    {
        mName = name
        mProUserId = proUserId
        mEmail = email
        mAvatar = avatar
        mPrice = price
        
        mDates = dates
        mLocationAddr = locationAddr
        mLocationComp = locationComp
        mLocationCoord = locationCoord
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //
        ImgUtil.load(url: mAvatar, uiAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        uiTitle.text = mName
        uiDesc1.text = mEmail
        uiDesc2.text = mPrice
        
        uiPrice.text = calculateTotalDesc()
        
        let cardNo: String? = Repo.pref.get("card@no")
        uiCardWrapper.isVisible = cardNo != nil
        uiCardEmpty.isVisible = cardNo == nil
        
        uiCardNo.text = Str.obfuscateCardNo(cardNo)
        uiCardValid.text = Repo.pref.get("card@valid")
        uiCardCvv.text = Str.obfuscateCardCvv(Repo.pref.get("card@cvv"))
        uiCardName.text = Repo.pref.get("card@name")
        
        uiCardBtn.setTitle(
            (cardNo == nil ? "ADICIONAR" : "ALTERAR") + " CARTÃO",
            for: .normal
        )
    }
    
    private func calculateTotalDesc() -> String
    {
        let price = Num.fromBrCurrency(mPrice, def: 0.0)
        let mins = mDates.reduce(0) { acc, i in acc + i.duration }
        
        mTotal = price * Double(mins) / 60.0
        
        return "Qtde de aulas: \(mDates.count) (\(mins) mins)\nHora aula: R$ \(mPrice!)\nTotal: R$ \(Str.number(mTotal, digits: 2))"
    }
    
    @IBAction func onCardChange(_ sender: Any) {
        guard let vc = UIStoryboard(name: "CardEditDialog", bundle: nil).instantiateInitialViewController() else { return }
        navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func onSubmit()
    {
        //
        let err = validate()
        Notify.cError = err
        if err != nil { return }
        
        //
        let loading = "Criando proposta..."
        if Notify.cLoading == loading { return }
        Notify.cLoading = loading
        
        //
        let params = FormCard(
            no: cCard!.no, valid_month: cCard!.validMonth(), valid_year: cCard!.validYear(),
            secret: cCard!.cvv, name: cCard!.name
        )
        Http.shared.post(ProposalCardTokenRModel.self, uri: "proposals/card-token", params)
            .then { [weak self] model in
                
                Task.main(delayInSeconds: 0.1) {

                    self?.afterSubmit(cardToken: model.payload.token)
                        .then(cb: { _ in
                            
                            self?.afterSuccess()
                            
                        })
                        .catchIt(cb: { reason in
                            Notify.cError = reason
                        })
                        .done {
                            Notify.cLoading = nil
                        }
                }
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }
    }
    
    private func afterSubmit(cardToken: String) -> PromiseEx<BaseRModel>
    {
        let jsonBody = ProposalBody.toJson(
            dates: mDates, address: mLocationAddr, addressComp: mLocationComp, coord: mLocationCoord
        )
        let params = FormProposal(
            pro_user_id: mProUserId, body: jsonBody, customer_name: cCard!.name,
            card_token: cardToken, price: mTotal, due_at: generateDueAt(mDates)
        )
        
        return Http.shared.post(BaseRModel.self, uri: "proposals", params)
            .then { [weak self] model in
                self?.afterSuccess()
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done {
                Notify.cLoading = nil
            }
    }
    
    private func afterSuccess() {
        performSegue(withIdentifier: "sgToDone", sender: nil)
    }
    
    private func generateDueAt(_ dates: [ProposalMultiDialogVC.Item]) -> String {
        let lastDt = dates.reduce(dates.first!.dt) { acc, i in acc > i.dt ? acc : i.dt }
        return Carbon(lastDt).add(2, .hour).formatSql()
    }
    
    private func validate() -> String? {
        if mTotal <= 0 { return "Total zero não é permitido para compra" }
        if cCard == nil { return "Adicione um cartão de crédito válido para finalizar contratação." }
        return nil
    }
    
    // card
    private struct Card {
        var no: String
        var valid: String
        var cvv: String
        var name: String
        
        func validMonth() -> String {
            return String(valid.split(separator: "/")[0])
        }
        func validYear() -> String {
            return String(valid.split(separator: "/")[1])
        }
    }
    
    private struct FormCard: Encodable {
        var no: String
        var valid_month: String
        var valid_year: String
        var secret: String
        var name: String
    }
    
    private struct FormProposal: Encodable {
        var pro_user_id: Int
        var body: ProposalBody.Body
        var customer_name: String
        var card_token: String
        var price: Double
        var due_at: String
    }
    
}


// MARK: scroll view
extension ProProposalSecondVC: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        BaseVC.scrollViewDidScroll(
            parentView: view, scrollView: scrollView, uiHeaderWrapper: uiHeaderWrapper, uiHeaderTopCon: uiHeaderTopCon
        )
    }
    
}
