//
//  ProProposalDoneVC.swift
//  conecte
//
//  Created by Adonio Silva on 14/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ProProposalDoneVC: UIViewController {
    
    
    @IBAction func onDone(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func navigateToProposals() {
        dismiss(animated: true) {
            DrawerVC.shared?.openProposal()
        }
    }
    
}
