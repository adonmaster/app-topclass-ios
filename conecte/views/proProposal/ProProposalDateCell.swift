//
//  ProProposalDateCell.swift
//  conecte
//
//  Created by Adonio Silva on 12/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ProProposalDateCell: UITableViewCell {
    
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiBtnClose: UIButton!
    
    private var index = -1
    private weak var delegate: ProProposalDateCellDelegate?

    @IBAction func onClose(_ sender: Any) {
        delegate?.proProposalDateCellClose(index: index)
    }
    
    func setDelegate(delegate: ProProposalDateCellDelegate, index: Int) {
        self.index = index
        self.delegate = delegate
    }
    
}

protocol ProProposalDateCellDelegate: NSObject {
    func proProposalDateCellClose(index: Int)
}
