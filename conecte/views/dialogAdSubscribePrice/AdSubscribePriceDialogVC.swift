//
//  DialogAdSubscribePriceVC.swift
//  conecte
//
//  Created by Adonio Silva on 30/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class AdSubscribePriceDialogVC: UIViewController {
    
    //
    @IBOutlet var uiPrice: UILabel!
    
    //
    var delegate: AdSubscribePriceDialogDelegate? = nil
    
    //
    private var mPrice: Double = 0.0 {
        didSet {
            uiPrice.text = "R$ " + Str.number(mPrice)
        }
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mPrice = Repo.pref.get("adSubscribePriceDialog@price", def: 0.0)
    }
    
    @IBAction func addPlus10() {
        inc(factor: 10)
    }

    @IBAction func addPlus20() {
        inc(factor: 20)
    }

    @IBAction func addPlus50() {
        inc(factor: 50)
    }
    
    @IBAction func addMin10() {
        inc(factor: -10)
    }
    
    @IBAction func addMin20() {
        inc(factor: -20)
    }
    
    @IBAction func addMin50() {
        inc(factor: -50)
    }
    
    @IBAction func onCancel() {
        dismiss(animated: true)
    }
    
    @IBAction func onSubmit() {
        dismiss(animated: true) {
            Repo.pref.set("adSubscribePriceDialog@price", self.mPrice)
            self.delegate?.adSubscribePriceDialogOnSubmit(price: self.mPrice)
        }
    }

    //
    
    private func inc(factor: Double) {
        let r = mPrice + factor
        mPrice = r > 0 ? r : 0
    }
    
}

// MARK: delegate
protocol AdSubscribePriceDialogDelegate {
    func adSubscribePriceDialogOnSubmit(price: Double)
}
