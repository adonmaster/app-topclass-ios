//
//  LocationPickerList.swift
//  conecte
//
//  Created by Adonio Silva on 25/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import MapKit


class LocationPickerList: UIViewController {
    
    //
    @IBOutlet var uiTv: UITableView!
    @IBOutlet var uiTvEmpty: UILabel!
    @IBOutlet var uiQuery: UISearchBar!

    //
    var delegate: LocationPickerListDelegate!
    
    //
    private var mDataLocations = [MKMapItem]()
    
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiQuery.delegate = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        let s = delegate.locationPickerListGetQuery()
        uiQuery.text = s
        requestData(s: s)
    }
    
    private func requestData(s: String?)
    {
        if let s = s, !s.trimmed.isEmpty {
            let request = MKLocalSearch.Request()
            request.naturalLanguageQuery = s
            request.region = delegate.locationPickerListGetMapRegion()
            let search = MKLocalSearch(request: request)
            
            search.start { [weak self] r, err in
                guard let r = r else { return }
                self?.mDataLocations = r.mapItems
                self?.uiTv.reloadData()
            }
        }
    }
    
}


// MARK: search bar
extension LocationPickerList: UISearchBarDelegate {
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        requestData(s: searchText)
    }
}

// MARK: table
extension LocationPickerList: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let n = mDataLocations.count
        uiTvEmpty.isVisible = n == 0
        return n
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! LocationPickerCell
        let m = mDataLocations[indexPath.row].placemark
        
        cell.textLabel?.text = [m.subThoroughfare, m.thoroughfare]
            .compactMap({ $0 })
            .joined(separator: ", ")
        
        cell.detailTextLabel?.text = [m.locality, m.administrativeArea]
            .compactMap({ $0 })
            .joined(separator: ", ")
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        navigationController?.popViewController(animated: true)
        
        let coord = mDataLocations[indexPath.row].placemark.coordinate
        delegate.locationPickerList(on: coord)
    }
    
}


// MARK: delegate
protocol LocationPickerListDelegate {
    
    func locationPickerListGetQuery() -> String?
    func locationPickerListGetMapRegion() -> MKCoordinateRegion
    
    func locationPickerList(on coord: CLLocationCoordinate2D)
    
}
