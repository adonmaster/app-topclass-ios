//
//  LocationPicker.swift
//  conecte
//
//  Created by Adonio Silva on 25/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class LocationPicker: UIViewController {
    
    //
    private let REGION_SPAN_IN_METERS = 830.0

    //
    var delegate: LocationPickerDelegate? = nil
    
    //
    @IBOutlet var uiMap: MKMapView!
    @IBOutlet var uiQuery: UISearchBar!
    @IBOutlet var uiCenter: UIImageView!
    
    //
    private lazy var manager = CLLocationManager()
    private var mUserRegion: MKCoordinateRegion? = nil
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        manager.delegate = self
        manager.desiredAccuracy = kCLLocationAccuracyBest
        manager.requestWhenInUseAuthorization()
        
        //
        uiQuery.delegate = self
        
        //
        let lat = Repo.pref.get("LocationPicker@lastCoordLat", def: 0.0)
        let lng = Repo.pref.get("LocationPicker@lastCoordLng", def: 0.0)
        if lat != 0.0 && lng != 0.0 {
            let coord = CLLocationCoordinate2D(latitude: lat, longitude: lng)
            uiMap.setRegion(
                MKCoordinateRegion(
                    center: coord, latitudinalMeters: REGION_SPAN_IN_METERS, longitudinalMeters: REGION_SPAN_IN_METERS
                ),
                animated: true
            )
        }
    }
    
    @IBAction func onCancel() {
        dismiss(animated: true) {
            //
        }
    }
    
    @IBAction func onSubmit() {
        Notify.cLoading = "Processando localização..."
        
        let center = uiMap.centerCoordinate
        let location = CLLocation(latitude: center.latitude, longitude: center.longitude)
        CLGeocoder().reverseGeocodeLocation(location) { [weak self] list, err in
            Notify.cLoading = nil
            
            if let place = list?.first {
                
                self?.reallySubmit(location.coordinate, place)
                
            } else if let err = err {
                Notify.cError = err.localizedDescription
                return
            } else {
                Notify.cError = "Erro de localizção..."
            }
        }
    }
    
    private func reallySubmit(_ coord: CLLocationCoordinate2D, _ place: CLPlacemark) {
        
        // center image disappear
        uiCenter.isVisible = false
        
        // remove annotations
        uiMap.removeAnnotations(uiMap.annotations)
        
        // add annotation
        let ann = MKPointAnnotation()
        ann.title = "Localização"
        ann.coordinate = coord
        uiMap.addAnnotation(ann)

        // dialog to confirm
        Task.main(delayInSeconds: 0.6) {
            
            let address = [
                    place.thoroughfare, place.subThoroughfare, place.locality, place.administrativeArea
                ].compactMap({ $0 })
                .joined(separator: ", ")

            Dialog(sender: self)
                .ok().cancel().title("Atenção")
                .msg("Deseja mesmo utilizar a localização apontada?\n\n\(address)")
                .present(onOk: {

                    self.dismiss(animated: true) {
                        self.delegate?.locationPicker(
                            onLocation: coord.latitude, lng: coord.longitude, address: address
                        )
                    }
                    
                    
                }, onCancel: {
                    
                    // remove annotations
                    self.uiMap.removeAnnotations(self.uiMap.annotations)
                    
                    // center image disappear
                    self.uiCenter.isVisible = true
                    
                }, onDestructive: nil, nil)
            
        }
        
    }
    
    @IBAction func onToUserRegion() {
        if let region = mUserRegion {
            uiMap.setRegion(region, animated: true)
        }
    }
    
    private func updateLocation() {
        manager.requestLocation()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? LocationPickerList {
            vc.delegate = self
        }
    }
    
}


// MARK: location manager delegate
extension LocationPicker: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedAlways || status == .authorizedWhenInUse {
            Notify.cError = nil
            
            self.updateLocation()
        } else {
            Notify.cError = "É necessário acesso à sua localização, veja suas permissões na configuração do seu IPhone."
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation])
    {
        guard let l = locations.first else { return }
        
        let region = MKCoordinateRegion(
            center: l.coordinate, latitudinalMeters: REGION_SPAN_IN_METERS, longitudinalMeters: REGION_SPAN_IN_METERS
        )
        uiMap.setRegion(region, animated: true)
        
        // cache
        Repo.pref.set("LocationPicker@lastCoordLat", l.coordinate.latitude)
        Repo.pref.set("LocationPicker@lastCoordLng", l.coordinate.longitude)
        
        // cache internally
        mUserRegion = region
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        Notify.cError = error.localizedDescription
    }
}

// MARK: list
extension LocationPicker: LocationPickerListDelegate {
    
    func locationPickerListGetQuery() -> String? {
        return uiQuery.text
    }
    
    func locationPickerListGetMapRegion() -> MKCoordinateRegion {
        return uiMap.region
    }
    
    func locationPickerList(on coord: CLLocationCoordinate2D) {
        let region = MKCoordinateRegion(
            center: coord, latitudinalMeters: REGION_SPAN_IN_METERS, longitudinalMeters: REGION_SPAN_IN_METERS
        )
        uiMap.setRegion(region, animated: true)
    }
    
}


// MARK: search bar
extension LocationPicker: UISearchBarDelegate {
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        performSegue(withIdentifier: "toList", sender: nil)
    }
    
}


protocol LocationPickerDelegate {
    
    func locationPicker(onLocation lat: Double, lng: Double, address: String)
    
}
