//
//  AdVC.swift
//  conecte
//
//  Created by Adonio Silva on 27/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class AdVC: BaseVC {
    
    @IBOutlet var uiHeaderWrapper: UIView!
    @IBOutlet var uiHeaderTopCon: NSLayoutConstraint!
    @IBOutlet var uiTv: UITableView!
    @IBOutlet var uiSegment: UISegmentedControl!
    @IBOutlet var uiEmpty: UIView!
    
    //
    private var loading = false
    private var mModel: AdRModel!
    private var mData = [AdRModel.Item]()
    private var tvLast: AdRModel.Item!
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        uiTv.tableFooterView = UIView()
        uiTv.initRefresher(caller: self, action: #selector(onRefresh(_:)))
        
        //
        uiSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Cl.mc.whiteColor()], for: .normal)
        uiSegment.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: Cl.mc.blackColor()], for: .selected)
    }
    
    @objc func onRefresh(_ sender: Any) {
        updateData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateData()
    }
    
    
    @IBAction func onSegment(_ sender: Any) {
        updateDesktop()
    }
    
   
    private func updateData() {
        // halt
        if Session.shared.user == nil || loading {
            return
        }
        
        loading = true
        Notify.cError = nil
        uiTv.isRefreshAnimating = true
        
        Http.shared.get(AdRModel.self, uri: "ads", params: [:])
            .then { [weak self] model in
                
                self?.mModel = model
                self?.updateDesktop()
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }.done { [weak self] in
                self?.loading = false
                self?.uiTv.isRefreshAnimating = false
            }
    }
    
    private func updateDesktop()
    {
        //
        let full = mModel.payload
        let sessionRemoteId = Session.shared.user?.remote_id ?? 0
        let mine = full.filter { m in
                return m.client_user_id == sessionRemoteId
                    || m.pros.contains(where: { pr in pr.pro_user.id == sessionRemoteId })
            }
        
        //
        uiSegment.setTitle("Todos (\(full.count))", forSegmentAt: 0)
        uiSegment.setTitle("Meus anúncios (\(mine.count))", forSegmentAt: 1)

        //
        mData = uiSegment.selectedSegmentIndex == 0 ? full : mine
        
        //
        uiTv.reloadData()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? AdDetailVC {
            vc.oModel = tvLast
        }
    }
    
    @IBAction func backToAd(_ segue: UIStoryboardSegue) {
        //
    }
    
}

// MARK: table view delegate
extension AdVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let n = mData.count
        uiEmpty.isHidden = n > 0
        return n
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AdCell
        let m = mData[indexPath.row]
        
        ImgUtil.load(url: m.client_user.avatar_thumb, cell.uiAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        
        cell.uiTitle.text = m.client_user.name
        cell.uiDesc.text = m.client_user.email

        let assignTo = Carbon.parseFromJson(m.body.items.first!.dt)!.formatBrDateTimeMin()
        let clazz = m.body.items.first!.materia
        let duration = m.body.items.first!.duration
        cell.uiDesc2.numberOfLines = 2
        cell.uiDesc2.text = "\(assignTo)\n\(clazz): \(duration) mins"
        
        cell.uiBadgeLbl.text = Str.plural(
            m.pros.count, "Nenhum profissional respondeu ainda", "Um profissional respondeu",
            { "\($0) profissionais responderam" }
        )
        cell.uiBadgeWrapper.backgroundColor = Cl.quad(Cl.mc.grey500Color(), [
            Cl.mc.blue500Color(): m.pros.count > 0
        ])
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if uiSegment.selectedSegmentIndex < 2 {
            tvLast = mData[indexPath.row]
            performSegue(withIdentifier: "sgToDetail", sender: nil)
        }
          
        tableView.deselectRow(at: indexPath, animated: true)
    }
      
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        BaseVC.scrollViewDidScroll(
            parentView: view, scrollView: scrollView, uiHeaderWrapper: uiHeaderWrapper,
            uiHeaderTopCon: uiHeaderTopCon, forceHiding: true
        )
    }
    
}
