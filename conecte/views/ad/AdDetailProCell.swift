//
//  AdDetailProCell.swift
//  conecte
//
//  Created by Adonio Silva on 29/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class AdDetailProCell: UICollectionViewCell {
    
    @IBOutlet var uiAvatar: UIImageView!
    @IBOutlet var uiTitle: UILabel!
    @IBOutlet var uiDesc: UILabel!
    @IBOutlet var uiPrice: UILabel!
    
}

