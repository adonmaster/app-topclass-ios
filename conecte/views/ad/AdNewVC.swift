//
//  AdNewVC.swift
//  conecte
//
//  Created by Adonio Silva on 27/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class AdNewVC: UIViewController {
    
    //
    @IBOutlet weak var uiHorTv: UITableView!
    @IBOutlet weak var uiHorTvHeight: NSLayoutConstraint!
    @IBOutlet weak var uiHorEmpty: UILabel!
    @IBOutlet weak var uiHorInfo: UILabel!
    
    @IBOutlet weak var uiLocEmpty: UILabel!
    @IBOutlet weak var uiLocWrapper: UIView!
    @IBOutlet weak var uiLocComp: UITextField!
    @IBOutlet weak var uiLocInfo: UILabel!
    
    //
    private var mDates = [ProposalMultiDialogVC.Item]()
    private var mLocation: SimpleAddress? = nil
    
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiLocWrapper.isVisible = false
    }
    
    @IBAction func onDtAdd(_ sender: Any) {
        let vc = UIStoryboard(name: "ProposalMultiDialog", bundle: nil).instantiateInitialViewController()
        if let vc = vc as? ProposalMultiDialogVC {
            vc.delegate = self
            vc.oClasses = Res.classes
            vc.oHorarios = []//oUser.p.horariosList
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
    
    @IBAction func onLocation(_ sender: Any)
    {
        performSegue(withIdentifier: "sgToLocationPicker", sender: nil)
    }
    
    struct Form: Encodable {
        var body: ProposalBody.Body
        var expire_at: String
    }
    
    
    @IBAction func onSubmit(_ sender: Any)
    {
        let err = validate()
        Notify.cError = err
        if err != nil { return }

        //
        let msg = "Enviando dados de anúncio..."
        if Notify.cLoading == msg { return }
        Notify.cLoading = msg
        
        //
        let jsonBody = ProposalBody.toJson(
            dates: mDates, address: mLocation!.address, addressComp: uiLocComp.text,
            coord: (mLocation!.lat, mLocation!.lng)
        )
        let expire_at = Carbon(mDates.map({ $0.dt }).min { $0 < $1 }!).formatSqlDate()
        let form = Form(body: jsonBody, expire_at: expire_at)
        Http.shared.post(BaseRModel.self, uri: "ads", form)
            .then { [weak self] _ in
                
                self?.afterSubmitted()
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done {
                Notify.cLoading = nil
            }
    }
    
    private func afterSubmitted() {
        performSegue(withIdentifier: "unwindAd", sender: nil)
    }
    
    private func validate() -> String? {
        if mDates.count <= 0 { return "Insira um horário antes de enviar" }
        if mLocation == nil { return "Escolha a localização da aula antes de enviar" }
        if mLocation!.address.range(of: "goiânia", options: .caseInsensitive) == nil {
            return "Atualmente só aceitamos localizações em Goiânia. Aguarde atualizações!"
        }
        return nil
    }
    
    private func updateDesktopLocation() {
        uiLocEmpty.isVisible = mLocation == nil
        uiLocWrapper.isVisible = mLocation != nil
        if let location = mLocation {
            uiLocInfo.text = location.address
        }
    }
    
    private func horTvReloadData() {
        uiHorTv.reloadData()
        Task.main(delayInSeconds: 0.2) {
            self.uiHorTvHeight.constant = self.uiHorTv.contentSize.height
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?)
    {
       if let nc = segue.destination as? UINavigationController,
           let vc = nc.topViewController as? LocationPicker
       {
           vc.delegate = self
       }
   }
    
    //
    struct SimpleAddress {
        var address: String
        var lat: Double
        var lng: Double
    }
    
}


// MARK: location picker
extension AdNewVC: LocationPickerDelegate {
    
    func locationPicker(onLocation lat: Double, lng: Double, address: String) {
        mLocation = SimpleAddress(address: address, lat: lat, lng: lng)
        updateDesktopLocation()
    }
    
}

// MARK: proposalmultidialogdelegate
extension AdNewVC: ProposalMultiDialogDelegate {

    func proposalMultiDialog(onDone res: ProposalMultiDialogVC.Item) {
        mDates.append(res)
        horTvReloadData()
    }
    
}

// MARK: table view hor
extension AdNewVC: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let n = mDates.count
        //
        uiHorTv.isVisible = n > 0
        uiHorEmpty.isVisible = n == 0
        
        //
        uiHorInfo.text = Str.plural(n, "sem aulas programadas", "apenas uma aula programada", { "\($0) aulas programadas" })
        
        //
        return n
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! AdNewHorCell
        
        let i = indexPath.row
        let m = mDates[i]
        
        cell.setDelegate(delegate: self, index: i)
        
        cell.uiTitle.text = Carbon(m.dt).format(pattern: "dd/MM/yyyy HH:mm")
            + "\n\(m.materia) - \(m.duration) mins"
        
        return cell
    }
    
    
}

// MARK: hor delegate
extension AdNewVC: AdNewHorCellDelegate {

    func adNewHorCellOnClose(index: Int) {
        mDates.remove(at: index)
        horTvReloadData()
    }
    
}
