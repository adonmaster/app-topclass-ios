//
//  AdCell.swift
//  conecte
//
//  Created by Adonio Silva on 27/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class AdCell: UITableViewCell {
    
    @IBOutlet var uiAvatar: UIImageView!
    @IBOutlet var uiTitle: UILabel!
    @IBOutlet var uiDesc: UILabel!
    @IBOutlet var uiDesc2: UILabel!
    @IBOutlet var uiBadgeWrapper: ViewEx!
    @IBOutlet var uiBadgeLbl: UILabel!
    
}
