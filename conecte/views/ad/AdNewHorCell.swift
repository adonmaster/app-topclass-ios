//
//  AdNewHorCel.swift
//  conecte
//
//  Created by Adonio Silva on 27/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class AdNewHorCell: UITableViewCell {
    
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiBtnClose: UIButton!
    
    private var index = -1
    private weak var delegate: AdNewHorCellDelegate?

    @IBAction func onClose(_ sender: Any) {
        delegate?.adNewHorCellOnClose(index: index)
    }
    
    func setDelegate(delegate: AdNewHorCellDelegate, index: Int) {
        self.index = index
        self.delegate = delegate
    }
    
}

protocol AdNewHorCellDelegate: NSObject {
    func adNewHorCellOnClose(index: Int)
}
