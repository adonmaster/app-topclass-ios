//
//  AdDetailVC.swift
//  conecte
//
//  Created by Adonio Silva on 29/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import MapKit

class AdDetailVC: UIViewController {
    
    
    //
    @IBOutlet weak var uiId: UILabel?
    
    @IBOutlet weak var uiAulaTitle: UILabel!
    @IBOutlet weak var uiAula: UILabel!
    
    @IBOutlet weak var uiMap: MKMapView!
    @IBOutlet weak var uiLocation: UILabel!
    
    @IBOutlet weak var uiProheader: UILabel!
    @IBOutlet weak var uiCvPros: UICollectionView!
    @IBOutlet weak var uiEmptyPros: UILabel!

    @IBOutlet weak var uiBtnSubscribe: UIButton!
    @IBOutlet weak var uiBtnUnsubscribe: UIButton!
    @IBOutlet weak var uiBtnDestroy: UIButton!

    
    //
    var oModel: AdRModel.Item!
    
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupDesktop()
    }
    
    private func setupDesktop() {
        //
        let idText = [
            "Anúncio #\(oModel.id)",
            "Criação \(oModel.p.createdAt.format(pattern: "dd/MM/yyyy HH:mm"))"
        ]
        uiId?.text = idText.joined(separator: "\n")
        
        //
        uiAulaTitle.text = Str.plural(oModel.body.items.count, "", "3. Aula", { "3. Aulas (\($0))" })
        
        //
        let aulaText = Span("")
        oModel.body.items.enumerated().forEach { (i: Int, item: AdRModel.BodyV2Item) in
            if aulaText.count > 0 { aulaText.append("\n\n") }
            
            let dt = Carbon.parseFromJson(item.dt)!
            let dtHuman = dt.diffForHuman()
            let duration = item.duration
            let materia = item.materia
            aulaText.append("\(i+1). ").boldSystem(size: 15.0).append("\(dt) (\(dtHuman))")
                .append("\n\(materia)")
                .append("\nDuração: \(duration) mins").color(Cl.mc.grey500Color())
        }
        uiAula.attributedText = aulaText.get()
        
        
        //
        uiLocation.text = [oModel.body.address_line, oModel.body.address_comp]
            .compactMap { $0 }.joined(separator: "\n")
        
        //
        uiMap.delegate = self
        setupMap(
            CLLocationCoordinate2D(latitude: oModel.body.address_lat, longitude: oModel.body.address_lng),
            oModel.client_user.name
        )
        
        //
        uiCvPros.reloadData()
        
        //
        let userId = Int(Session.shared.user?.remote_id ?? 0)
        
        //
        uiBtnSubscribe.isVisible = Session.shared.user?.is_pro == true
        uiBtnSubscribe.setTitle(
            oModel.p.isUserSomePro(userId: userId) ? "REDEFINIR PREÇO DO ANÚNCIO" : "RESPONDER ANÚNCIO",
            for: .normal
        )
        
        //
        uiBtnUnsubscribe.isVisible = oModel.p.isUserSomePro(userId: userId)
        
        
        //
        uiBtnDestroy.isVisible = oModel.p.isUserClient(userId: userId)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // title
        navigationItem.title = oModel.client_user.name
    }
    
    private func setupMap(_ coord: CLLocationCoordinate2D, _ title: String) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coord
        annotation.title = title

        let region = MKCoordinateRegion(center: coord, latitudinalMeters: 1200, longitudinalMeters: 1200)
        
        uiMap.setRegion(region, animated: true)
        uiMap.addAnnotation(annotation)
    }
    
    private func requestData() {
        Notify.cLoading = "Carregando..."
        Http.shared.get(AdSingleRModel.self, uri: "ad/\(oModel.id)", params: [:])
            .then {[weak self] m in
                self?.oModel = m.payload
                self?.setupDesktop()
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done {
                Notify.cLoading = nil
            }

    }
    
    
    @IBAction func onSubscribe(_ sender: Any) {
        performSegue(withIdentifier: "sgToPriceDialog", sender: nil)
    }
    
    //
    struct SubscribeForm: Encodable {
        var price: Double
    }
    private func reallySubscribe(_ price: Double) {
        if Notify.cLoading != nil { return }
        
        Notify.cLoading = "Enviando dados..."
        let form = SubscribeForm(price: price)
        Http.shared.post(BaseRModel.self, uri: "ads/\(oModel.id)/subscribe", form)
            .then { [weak self] _ in
                self?.requestData()
            }
            .catchIt { reason in
                Notify.cLoading = nil
                Notify.cError = reason
            }
    }
    
    @IBAction func onUnsubscribe(_ sender: Any) {
        if Notify.cLoading != nil { return }
        
        Notify.cLoading = "Removend anúncio..."
        Http.shared.post(BaseRModel.self, uri: "ads/\(oModel.id)/unsubscribe", BaseForm())
            .then {[weak self] _ in
                self?.requestData()
            }
            .catchIt { reason in
                Notify.cLoading = nil
                Notify.cError = reason
            }
    }
    
    @IBAction func onDestroy(_ sender: Any) {
        Dialog(sender: self).ok().cancel()
            .title("Atenção").msg("Tem certeza que deseja remover?")
            .present(onOk: {
                self.reallyDestroy()
            }, onCancel: nil, onDestructive: nil, nil)
    }
    
    private func reallyDestroy() {
        Notify.cLoading = "Removendo..."
        Http.shared.postDelete(BaseRModel.self, uri: "ads/\(oModel.id)", BaseForm())
            .then {[weak self] _ in
                self?.performSegue(withIdentifier: "unwindToAdMain", sender: nil)
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done {
                Notify.cLoading = nil
            }
    }
    
    //
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? AdSubscribePriceDialogVC {
            vc.delegate = self
        }
    }
    
}

// MARK: collection
extension AdDetailVC: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let n = oModel.pros.count
        uiEmptyPros.isHidden = n > 0
        uiCvPros.isHidden = n == 0
        uiProheader.text = "Profissionais" + (n == 0 ? "" : " (\(n))")
        return n
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! AdDetailProCell
        let m = oModel.pros[indexPath.row]
        
        ImgUtil.load(url: m.pro_user.avatar_thumb, cell.uiAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        cell.uiTitle.text = m.pro_user.name
        cell.uiDesc.text = m.pro_user.email
        cell.uiPrice.text = "R$ " + Str.number(m.price)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        Dialog(sender: self)
            .stylesheet(popoverSourceView: view)
            .title("Anúncio").msg("Escolha ação abaixo para o profissional")
            .action(key: "Definir preço do anúncio") { _ in
                self.onSubscribe(self)
            }
            .action(key: "Remover preço do anúncio") { _ in
                self.onUnsubscribe(self)
            }
            .cancel()
            .present()
    }
    
}



// MARK:  map
extension AdDetailVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let ann = view.annotation {
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: ann.coordinate))
            mapItem.name = ann.title ?? "Conecte Aulas"
            mapItem.openInMaps(launchOptions: [:])
        }
    }
    
}

// MARK: price delegate
extension AdDetailVC: AdSubscribePriceDialogDelegate {

    func adSubscribePriceDialogOnSubmit(price: Double) {
        reallySubscribe(price)
    }
    
}
