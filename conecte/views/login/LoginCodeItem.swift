//
//  LoginCodeItem.swift
//  conecte
//
//  Created by Adonio Silva on 25/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class LoginCodeItem: UIViewController {
    
    //
    @IBOutlet weak var uiCode: TextFieldEx!
    @IBOutlet weak var uiDesc: UILabel!
    
    //
    var oName: String!
    var oEmail: String!
    
    //
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        uiDesc.text = "Foi enviado um código para\n\(oEmail!).\nInsira-o abaixo"
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        Notify.cError = nil
        switch validate() {
        case .success(let form):
            reallySubmit(form)
        case .failure(let reason):
            Notify.cError = reason
        }
    }
    
    private func reallySubmit(_ form: Form) {
        let loading = "Enviando dados de login..."
        if Notify.cLoading == loading { return }
        
        Notify.cLoading = loading
        Http.shared.post(LoginCodeRModel.self, uri: "register/code", form)
            .then { [weak self] model in
                
                self?.afterSubmit(model)
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done {
                Notify.cLoading = nil
            }
    }
    
    private func afterSubmit(_ model: LoginCodeRModel) {
        Session.shared.saveUser(
            remote_id: model.payload.id,
            name: oName, email: oEmail, avatar: model.payload.avatar,
            token: model.payload.api_token,
            isPro: model.payload.is_pro
        )
        
        LoginPage.shared?.navigateToEnd()
    }
    
    private func validate() -> ResultEx<Form> {
        let code = uiCode.text
        if Str.isEmpty(code) { return .failure("É necessário preencher o código.") }
        
        return .success(Form(email: self.oEmail, code: code!))
    }
    
    // MARK: Form
    struct Form: Encodable {
        var email: String
        var code: String
    }
    
}
