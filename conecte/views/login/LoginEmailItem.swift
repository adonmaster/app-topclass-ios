//
//  LoginEmailItem.swift
//  conecte
//
//  Created by Adonio Silva on 25/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class LoginEmailItem: UIViewController {
    
    @IBOutlet weak var uiName: TextFieldEx!
    @IBOutlet weak var uiEmail: TextFieldEx!
    

    //
    @IBAction func onSubmit(_ sender: Any) {
        Notify.cError = nil
        switch validate() {
        case .success(let form):
            self.reallySubmit(form)
        case .failure(let reason):
            Notify.cError = reason
        }
    }
    
    private func reallySubmit(_ form: Form) {
        let msg = "Enviando dados de login..."
        if Notify.cLoading == msg { return }
        
        Notify.cLoading = msg
        Http.shared.post(RModel.self, uri: "register", form)
            .then { _ in
                
                LoginPage.shared?.navigateToCode(name: form.name, email: form.email)
                
            }
            .catchIt { err in
                Notify.cError = err
            }
            .done {
                Notify.cLoading = nil
            }
    }
    
    private func validate() -> ResultEx<Form> {
        let name = self.uiName.text
        let email = self.uiEmail.text

        if Str.isEmpty(name) { return .failure("Você deve preencher o seu nome") }
        if Str.isEmpty(email) { return .failure("Você deve preencher o seu email") }
        
        return .success(Form(name: name!.trimmed, email: email!.trimmed))
    }
    
    // MARK: form
    struct Form: Encodable {
        var name: String
        var email: String
    }
    
    struct RModel: Decodable {
        var message: String
    }
}


