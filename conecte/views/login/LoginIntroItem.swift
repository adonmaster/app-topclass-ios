//
//  LoginIntroItem.swift
//  conecte
//
//  Created by Adonio Silva on 25/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class LoginIntroItem: UIViewController {
    
    @IBOutlet weak var uiDesc: UILabel!
    
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiDesc.text = "Versão \(App.i.version)"
        
        Task.main(delayInSeconds: 1.5) {
            
            LoginPage.shared?.navigateToEmail()
            
        }
    }
    
}
