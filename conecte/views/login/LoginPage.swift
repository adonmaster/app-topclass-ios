//
//  LoginPage.swift
//  conecte
//
//  Created by Adonio Silva on 25/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit


class LoginPage: UIPageViewController {
    
    static weak var shared: LoginPage? = nil
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        LoginPage.shared = self
        
        delegate = self
        dataSource = nil
        
        navigateTo(storyboardName: "intro", animated: false)
        
    }
    
    @discardableResult private func navigateTo(storyboardName: String, animated: Bool) -> UIViewController {
        let vc = storyboard!.instantiateViewController(withIdentifier: storyboardName)
        setViewControllers(
            [vc], direction: .forward, animated: animated, completion: nil
        )
        return vc
    }
    
    func navigateToEmail() {
        navigateTo(storyboardName: "email", animated: true)
    }
    
    func navigateToCode(name: String, email: String) {
        let vc = navigateTo(storyboardName: "code", animated: true) as! LoginCodeItem
        vc.oEmail = email
        vc.oName = name
    }
    
    func navigateToEnd() {
        dismiss(animated: true, completion: nil)
    }
    
}

// MARK: delegate
extension LoginPage: UIPageViewControllerDelegate {
    
}
