//
//  ProfileProRequisitionVC.swift
//  conecte
//
//  Created by Adonio Silva on 14/09/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import MobileCoreServices

class ProfileProRequisitionVC: UIViewController {
    
    //
    @IBOutlet var uiUploadIcon: UILabel!
    @IBOutlet var uiObs: UITextView!
    
    private var mPdf: Data? = nil {
        didSet {
            uiUploadIcon?.textColor = mPdf == nil
                ? Cl.mc.blue500Color()
                : Cl.mc.green500Color()
        }
    }

    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mPdf = nil
    }
    
    //
    
    private func resetDesktop() {
        mPdf = nil
        uiObs.text = ""
    }
    
    @IBAction func onUpload(_ sender: Any) {
        let vc = UIDocumentPickerViewController(documentTypes: [(kUTTypePDF as String)], in: .import)
        vc.delegate = self
        vc.modalTransitionStyle = .coverVertical
        present(vc, animated: true, completion: nil)
    }
    
    @IBAction func onSubmit(_ sender: Any) {
        if Notify.cLoading != nil { return }
        guard let pdf = mPdf else {
            Notify.cError = "Escolha um arquivo pdf como currículo"
            return
        }
        
        Notify.cLoading = "Enviando dados..."
        Http.shared.upload(BaseRModel.self, uri: "pro_requisition", params: [
                "obs": .string(uiObs.text),
                "cv": .pdf(pdf)
            ])
            .then {[weak self] _ in
                self?.resetDesktop()
                let msg = "Requisição enviada com sucesso.\nAguarde e será notificado."
                Notify.info(msg: msg, title: "Requisição", position: .top, displayDuration: .infinity)
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done {
                Notify.cLoading = nil
            }
    }
    
}


// MARK: document picker
extension ProfileProRequisitionVC: UIDocumentPickerDelegate {
    
    func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentsAt urls: [URL]) {
        guard let url = urls.first else { return }
        mPdf = try? Data(contentsOf: url)
    }
    
}
