//
//  ProfileDetailVC.swift
//  conecte
//
//  Created by Adonio Silva on 06/09/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import CropViewController

class ProfileDetailVC: UIViewController {

    @IBOutlet var uiAvatar: UIImageView!
    @IBOutlet var uiEdName: UITextField!
    @IBOutlet var uiEdEmail: UITextField!

    
    private var mNewPhoto: UIImage? = nil {
        didSet {
            if let img = mNewPhoto {
                uiAvatar.image = img
            }
        }
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        guard let user = Session.shared.user else { return }
        
        ImgUtil.load(url: user.avatar, uiAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        uiEdName.text = user.name
        uiEdEmail.text = user.email
        
        mNewPhoto = nil
    }
    
    @IBAction func onAvatar(_ sender: Any) {
        Dialog(sender: self).cancel()
            .stylesheet(popoverSourceView: view)
            .msg("Escolha fonte para escolher sua foto:")
            .action(key: "Album", cb: { _ in
                ImgProviderNative.i.presentPicker(caller: self, .photoLibrary)
            })
            .action(key: "Câmera") { _ in
                ImgProviderNative.i.presentPicker(caller: self, .camera)
            }
            .present()
    }
    
    @IBAction func onSubmit(_ sender: Any)
    {
        if Notify.cLoading != nil { return }
        
        var params: [String: Http.ParamValue] = [
            "name": .string(uiEdName.text ?? "[desconhecido]")
        ]
        if let newImage = mNewPhoto {
            params["avatar"] = .jpeg(newImage)
        }
        Notify.cLoading = "Enviando dados de perfil..."
        Http.shared.upload(UserRModel.self, uri: "profile", params: params)
            .then {[weak self] data in
                self?.afterSubmit(data.payload.user)
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done {
                Notify.cLoading = nil
            }
    }
    
    private func afterSubmit(_ user: UserRModel.Usr) {
        Session.shared.updateUser(email: user.email, name: user.name, avatar: user.avatar_thumb)
        Notify.info(msg: "Dados salvos com sucesso!")
    }
    
}

// MARK: photopicker
extension ProfileDetailVC: ImgProviderNativeDelegate {
    
    func imgProviderNative(onImgs imgs: [UIImage]) {
        mNewPhoto = imgs.first
    }
    
    func imgProviderNativeLimit() -> Img.LimitType? {
        return .width(800)
    }
    
    func imgProviderNativeAllowsEditing() -> Bool {
        return true
    }
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any])
    {
         ImgProviderNative.i.imagePickerController(self, picker, didFinishPickingMediaWithInfo: info)
     }
    
     func cropViewController(_ cropViewController: CropViewController, didCropToImage image: UIImage, withRect cropRect: CGRect, angle: Int)
     {
         ImgProviderNative.i.cropViewController(self, cropViewController, didCropToImage: image, withRect: cropRect, angle: angle)
     }
    
}
