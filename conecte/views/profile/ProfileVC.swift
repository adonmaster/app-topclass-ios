//
//  ProfileVC.swift
//  conecte
//
//  Created by Adonio Silva on 06/09/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit


class ProfileVC: BaseVC {
    
    //
    @IBOutlet var uiPager: ViewPager!
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func onSegment(_ segment: UISegmentedControl) {
        var i = segment.selectedSegmentIndex
        if (i == 1 && Session.shared.user?.is_pro != true) || true {
            i = 2
        }
        uiPager.swap(controllerIndex: i, animator: CrossDissolveViewPagerAnimator())
    }
    
}
