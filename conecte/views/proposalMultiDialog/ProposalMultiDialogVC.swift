//
//  ProposalMultiDialogVC.swift
//  conecte
//
//  Created by Adonio Silva on 11/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit


class ProposalMultiDialogVC: UIViewController {
    
    //
    var oClasses = [String]()
    var oHorarios = [(String, String)]()
    var delegate: ProposalMultiDialogDelegate?
    
    
    //
    @IBOutlet weak var uiDt: UIDatePicker!
    @IBOutlet weak var uiDtErrorInfo: UILabel!
    @IBOutlet weak var uiDuration: UISlider!
    @IBOutlet weak var uiDurationDesc: UILabel!
    @IBOutlet weak var uiClass: UIPickerView!
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiClass.dataSource = self
        uiClass.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        onDuration(uiDuration!)
        uiDt.locale = Locale.current
        uiDt.minimumDate = Carbon().add(12, .hour).get()
        uiDtErrorInfo.isVisible = false
    }
    
    @IBAction func onDt(_ sender: Any) {
        if let err = validateDt() {
            uiDtErrorInfo.text = err
            uiDtErrorInfo.isVisible = true
        } else {
            uiDtErrorInfo.isVisible = false
        }
    }
    
    private func validateDt() -> String?
    {
        if oHorarios.count > 0 && !UserFullPresenter.isClassDateValid(uiDt.date, haystack: oHorarios) {
            return "Data inválida, lembre-se que o professor selecionado tem horários:\n\n"
                + oHorarios
                    .map({ $0.0 + " " + $0.1 })
                    .joined(separator: ", ")
        }
        
        if !Carbon(uiDt.date).isFuture() {
            return "Data deve ser futura"
        }
        
        return nil
    }
    
    private func validate() -> String?
    {
        if let err = validateDt() {
            return err
        }
        
        if oClasses.get(uiClass.selectedRow(inComponent: 0)) == nil {
            return "Escolha uma máteria da lista"
        }
        
        return nil
    }
    
    @IBAction func onDuration(_ sender: Any) {
        let v = round(uiDuration.value / 30) * 30
        uiDuration.value = v
        
        uiDurationDesc.text = "\(Int(v)) mins"
    }
    
    @IBAction func onCancel(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func onOk(_ sender: Any) {
        Notify.cError = nil
        if let err = validate() {
            Notify.cError = err
            return
        }
        
        navigationController?.popViewController(animated: true)
        
        // delegate
        let dt = uiDt.date
        let duration = Int(uiDuration.value)
        let materia = oClasses[uiClass.selectedRow(inComponent: 0)]
        delegate?.proposalMultiDialog(onDone: ProposalMultiDialogVC.Item(dt: dt, duration: duration, materia: materia))
    }
    
    //
    struct Item {
        var dt: Date
        var duration: Int
        var materia: String
    }
}

// MARK: picker view delegate
extension ProposalMultiDialogVC: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return oClasses.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return oClasses[row]
    }
    
}


// MARK: delegate
protocol ProposalMultiDialogDelegate {
    
    func proposalMultiDialog(onDone res: ProposalMultiDialogVC.Item)
    
}
