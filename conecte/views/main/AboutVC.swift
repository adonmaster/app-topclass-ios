//
//  AboutVC.swift
//  conecte
//
//  Created by Adonio Silva on 01/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class AboutVC: BaseVC {
    
    @IBOutlet weak var uiVersion: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        uiVersion.text = "Versão \(App.i.version)"
    }
    
    @IBAction func openPro(_ sender: Any) {
        DrawerVC.shared?.openPro()
    }
    
    @IBAction func openProposal(_ sender: Any) {
        DrawerVC.shared?.openProposal()
    }
    
    @IBAction func openContract(_ sender: Any) {
        DrawerVC.shared?.openContract()
    }
    
    @IBAction func openTransactino(_ sender: Any) {
        DrawerVC.shared?.openTransaction()
    }
    
}
