//
//  DrawerVC.swift
//  conecte
//
//  Created by Adonio Silva on 01/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class DrawerVC: UIViewController {
    
    //
    static weak var shared: DrawerVC? = nil
    
    //
    @IBOutlet weak var uiAvatar: ImageViewRounded!
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiDesc: UILabel!
    @IBOutlet weak var uiTv: UITableView!
    
    
    //
    private let mData: [(String,String)] = [
        ("Início", "sgAbout"),
        ("Professores", "sgPro"),
        ("Propostas", "sgProposal"),
        ("Contratos", "sgContract"),
        ("Aula Emergencial", "sgAd"),
        ("Transações", "sgTransaction")
    ]
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Self.shared = self
        uiTv.tableFooterView = UIView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateDesktop()
    }
    
    private func updateDesktop() {
        guard let user = Session.shared.user else { return }
        
        //
        uiTitle.text = user.name
        
        //
        uiDesc.text = user.email
        
        //
        ImgUtil.load(url: user.avatar, uiAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
    }
   
    
    // open
    func openAbout() {
          performSegue(withIdentifier: "sgAbout", sender: nil)
    }
    func openPro() {
          performSegue(withIdentifier: "sgPro", sender: nil)
    }
    func openContract() {
          performSegue(withIdentifier: "sgContract", sender: nil)
    }
    func openProposal() {
          performSegue(withIdentifier: "sgProposal", sender: nil)
    }
    func openAd() {
        performSegue(withIdentifier: "sgAd", sender: nil)
    }
    func openTransaction() {
        performSegue(withIdentifier: "sgTransaction", sender: nil)
    }

}

extension DrawerVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! DrawerCell
        let m = mData[indexPath.row]
        
        cell.uiTitle.text = m.0.uppercased()
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let m = mData[indexPath.row]
        
        guard !m.1.isEmpty else { return }
        performSegue(withIdentifier: m.1, sender: nil)
    }
    
    
}
