//
//  MainVC.swift
//  conecte
//
//  Created by Adonio Silva on 25/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit


class MainVC: SlideMenuVC {
    
    override func mainStoryboardToLoad() -> String? {
        return Repo.pref.getLastStoryboard()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let user = Session.shared.user {
            
            AppDelegate.shared.configApns()
            
            updateDesktop(user)
            
        } else {
            
            performSegue(withIdentifier: "sgLogin", sender: nil)
            
        }
        
    }
    
    private func updateDesktop(_ user: User) {
        
    }
    
}
