//
//  TransactionCell.swift
//  conecte
//
//  Created by Adonio Silva on 31/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class TransactionCell: UITableViewCell {
    
    @IBOutlet var uiDesc1: UILabel!
    @IBOutlet var uiDesc2: UILabel!
    @IBOutlet var uiPrice1: UILabel!
    @IBOutlet var uiPrice2: UILabel!
    @IBOutlet var uiBadgeWrapper: UIView!
    @IBOutlet var uiBadge: UILabel!

    
}
