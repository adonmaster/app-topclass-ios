//
//  TransactionVC.swift
//  conecte
//
//  Created by Adonio Silva on 31/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class TransactionVC: BaseVC {
    
    //
    @IBOutlet var uiHeaderWrapper: UIView!
    @IBOutlet var uiHeaderTopCon: NSLayoutConstraint!
    @IBOutlet var uiTv: UITableView!
    @IBOutlet var uiEmpty: UIView!
    
    //
    private var loading = false
    private var mModel: TransactionRModel? = nil
    private var mData: [(String, [TransactionRModel.Item])]{
        get {
            let list: [TransactionRModel.Item] = mModel?.payload.data ?? []
            return Arr(list).categorize { m -> String in
                return m.p.createdAt.formatBrDate()
            }
        }
    }
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        uiTv.tableFooterView = UIView()
        uiTv.initRefresher(caller: self, action: #selector(onRefresh(_:)))
    }
    
    @objc func onRefresh(_ sender: Any) {
        updateData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateData()
    }
    
    private func updateData() {
        // halt
        if Session.shared.user == nil || loading {
            return
        }
        
        //
        loading = true
        Notify.cError = nil
        uiTv.isRefreshAnimating = true
        
        //
        Http.shared.get(TransactionRModel.self, uri: "transactions", params: [:])
            .then {[weak self] model in
                
                self?.mModel = model
                self?.updateDesktop()
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done { [weak self] in
                self?.loading = false
                self?.uiTv.isRefreshAnimating = false
            }
    }
    
    private func updateDesktop() {
        uiTv.reloadData()
    }
    
}


// MARK: table view
extension TransactionVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let n = mData[section].1.count
        uiEmpty.isVisible = n==0
        return n
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return mData.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return Carbon.parse(mData[section].0, "dd/MM/yyyy")?.format(pattern: "EEEE, dd MMM") ?? ""
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! TransactionCell
        let m = mData[indexPath.section].1[indexPath.row]
        
        //
        cell.uiDesc1.text = m.desc
        
        //
        let dtExt = m.p.createdAt.format(pattern: "dd/MM")
        let dtFrom = m.p.createdAt.diffForHuman()
        cell.uiDesc2.text = "\(dtFrom) (\(dtExt))"
        
        //
        let monitizer: (UILabel, Double, Bool)->() = { v, d, showCurrency in
            v.text = (showCurrency ? "R$ " : "") + Str.number(d)
            v.textColor = d < 0 ? Cl.mc.red500Color() : Cl.mc.green500Color()
        }
        
        //
        monitizer(cell.uiPrice1, m.amount, false)
        
        //
        monitizer(cell.uiPrice2, m.balance, true)
        
        //
        cell.uiBadge.text = m.p.operationTypeDesc
        cell.uiBadgeWrapper.backgroundColor = m.p.operationTypeCl
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        BaseVC.scrollViewDidScroll(
            parentView: view, scrollView: scrollView, uiHeaderWrapper: uiHeaderWrapper,
            uiHeaderTopCon: uiHeaderTopCon, forceHiding: true
        )
    }
}
