//
//  ContractDetailVC.swift
//  conecte
//
//  Created by Adonio Silva on 25/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import MapKit


class ContractDetailVC: UIViewController {
    
    //
    @IBOutlet weak var uiId: UILabel?
    
    @IBOutlet weak var uiProAvatar: ImageViewRounded!
    @IBOutlet weak var uiProTitle: UILabel!
    @IBOutlet weak var uiProDesc: UILabel!
    
    @IBOutlet weak var uiCliAvatar: ImageViewRounded!
    @IBOutlet weak var uiCliTitle: UILabel!
    @IBOutlet weak var uiCliDesc: UILabel!
    
    @IBOutlet weak var uiAulaTitle: UILabel!
    @IBOutlet weak var uiAula: UILabel!
    
    @IBOutlet weak var uiMap: MKMapView!
    @IBOutlet weak var uiLocation: UILabel!
    
    @IBOutlet weak var uiPrice: UILabel!
    
    @IBOutlet weak var uiInfoWrapper: UIStackView!
    @IBOutlet weak var uiInfoIcon: UILabel!
    @IBOutlet weak var uiInfoDesc: UILabel!
    
    //
    var oModel: ContractRModel.Item!
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        var idText = [
            "Contrato #\(oModel.id)",
            "Criada \(oModel.p.createdAt.format(pattern: "dd/MM/yyyy HH:mm"))",
            "--"
        ]
        oModel.body
            .items
            .enumerated()
            .forEach { tu in
                idText.append(
                    "Aula #\(tu.offset + 1) - \(tu.element.dtObject.format(pattern: "dd/MM/yyyy HH:mm"))"
                )
            }
        uiId?.text = idText.joined(separator: "\n")
        
        //
        ImgUtil.load(url: oModel.pro_user.avatar_thumb, uiProAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        uiProTitle.text = oModel.pro_user.name
        uiProDesc.text = oModel.pro_user.email
        
        //
        ImgUtil.load(url: oModel.client_user.avatar_thumb, uiCliAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))
        uiProTitle.text = oModel.client_user.name
        uiProDesc.text = oModel.client_user.email
        
        //
        uiAulaTitle.text = Str.plural(oModel.body.items.count, "", "3. Aula", { "3. Aulas (\($0))" })
        
        //
        let aulaText = Span("")
        oModel.body.items.enumerated().forEach { (i: Int, item: ContractRModel.Item.BodyV2Item) in
            if aulaText.count > 0 { aulaText.append("\n\n") }
            
            let dt = Carbon.parseFromJson(item.dt)!
            let dtHuman = dt.diffForHuman()
            let duration = item.duration
            let materia = item.materia
            aulaText.append("\(i+1). ").boldSystem(size: 15.0).append("\(dt) (\(dtHuman))")
                .append("\n\(materia)")
                .append("\nDuração: \(duration) mins").color(Cl.mc.grey500Color())
        }
        uiAula.attributedText = aulaText.get()
        
        
        //
        uiLocation.text = [oModel.body.address_line, oModel.body.address_comp]
            .compactMap { $0 }.joined(separator: "\n")
        
        //
        uiMap.delegate = self
        setupMap(
            CLLocationCoordinate2D(latitude: oModel.body.address_lat, longitude: oModel.body.address_lng),
            oModel.client_user.name
        )
        
        //
        uiPrice.text = "R$ " + Str.number(oModel.price)

        //
        let dueDt = oModel.p.dueAt.formatBrDate()
        let dueDesc: String
        let dueColor: UIColor
        let dueIcon: String
        if oModel.was_executed {
            dueDesc = "O contrato foi executado no dia \(dueDt)"
            dueColor = Cl.mc.green500Color()
            dueIcon = ""
        } else {
            dueDesc = "Se nenhuma objeção for encontrada, o contrato será executado automaticamente em \(dueDt), movimentando o pagamento do aluno para o professor"
            dueColor = Cl.mc.orange500Color()
            dueIcon = ""
        }
        uiInfoDesc.text = dueDesc
        uiInfoDesc.textColor = dueColor
        uiInfoIcon.text = dueIcon
        uiInfoIcon.textColor = dueColor
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        // title
        let usr = oModel.client_user.id == Int(Session.shared.user?.remote_id ?? 0)
            ? oModel.pro_user : oModel.client_user
        navigationItem.title = usr.name
    }
    
    
    private func setupMap(_ coord: CLLocationCoordinate2D, _ title: String) {
        let annotation = MKPointAnnotation()
        annotation.coordinate = coord
        annotation.title = title

        let region = MKCoordinateRegion(center: coord, latitudinalMeters: 1200, longitudinalMeters: 1200)
        
        uiMap.setRegion(region, animated: true)
        uiMap.addAnnotation(annotation)
    }
    
}

// MARK:  map
extension ContractDetailVC: MKMapViewDelegate {
    
    func mapView(_ mapView: MKMapView, didSelect view: MKAnnotationView) {
        if let ann = view.annotation {
            let mapItem = MKMapItem(placemark: MKPlacemark(coordinate: ann.coordinate))
            mapItem.name = ann.title ?? "Conecte Aulas"
            mapItem.openInMaps(launchOptions: [:])
        }
    }
    
}
