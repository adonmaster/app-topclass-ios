//
//  ContractVC.swift
//  conecte
//
//  Created by Adonio Silva on 17/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ContractVC: BaseVC {
    
    //
    @IBOutlet var uiHeaderWrapper: UIView!
    @IBOutlet var uiHeaderTopCon: NSLayoutConstraint!
    @IBOutlet var uiTv: UITableView!
    @IBOutlet var uiEmpty: UIView!
    
    //
    private var loading = false
    private var mModel: ContractRModel? = nil
    private var mData: [ContractRModel.Item]{
        get {
            return mModel?.payload ?? []
        }
    }
    private var mLastSelectedItem: ContractRModel.Item? = nil
    
    
    //
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //
        uiTv.tableFooterView = UIView()
        uiTv.initRefresher(caller: self, action: #selector(onRefresh(_:)))
    }
    
    @objc func onRefresh(_ sender: Any) {
        updateData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        updateData()
    }
    
    private func updateData() {
        // halt
        if Session.shared.user == nil || loading {
            return
        }
        
        //
        loading = true
        Notify.cError = nil
        uiTv.isRefreshAnimating = true
        
        //
        Http.shared.get(ContractRModel.self, uri: "contracts", params: [:])
            .then {[weak self] model in
                
                self?.mModel = model
                self?.updateDesktop()
                
            }
            .catchIt { reason in
                Notify.cError = reason
            }
            .done { [weak self] in
                self?.loading = false
                self?.uiTv.isRefreshAnimating = false
            }
    }
    
    private func updateDesktop() {
        uiTv.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let vc = segue.destination as? ContractDetailVC {
            vc.oModel = mLastSelectedItem
        }
    }
    
}

// MARK: table view
extension ContractVC: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let n = mData.count
        uiEmpty.isVisible = n==0
        return n
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ContractCell
        let m = mData[indexPath.row]
        
        let usr = m.client_user.id == Int(Session.shared.user?.remote_id ?? 0)
            ? m.pro_user : m.client_user

        //
        ImgUtil.load(url: usr.avatar_thumb, cell.uiAvatar, placeholder: #imageLiteral(resourceName: "img_avatar"))

        //
        cell.uiTitle.text = usr.name
        
        //
        cell.uiDesc.text = "R$ \(Str.number(m.price)), contrato n.o \(m.id)"
        
        //
        cell.uiDesc2.text = "\(m.p.dueAt.diffForHuman()), \(m.p.dueAt.formatBrDate())".capitalized
        
        //
        if m.was_executed {
            cell.uiBadgeWrapper.backgroundColor = Cl.mc.green500Color()
            cell.uiBadgeLbl.text = "Contrato executado"
        } else {
            cell.uiBadgeWrapper.backgroundColor = Cl.mc.orange500Color()
            cell.uiBadgeLbl.text = "Contrato ainda não executado"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath)
    {
        mLastSelectedItem = mData[indexPath.row]
        
        performSegue(withIdentifier: "sgToDetail", sender: nil)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        BaseVC.scrollViewDidScroll(
            parentView: view, scrollView: scrollView, uiHeaderWrapper: uiHeaderWrapper,
            uiHeaderTopCon: uiHeaderTopCon, forceHiding: true
        )
    }
}
