//
//  Res.swift
//  conecte
//
//  Created by Adonio Silva on 27/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class Res {
    
    static let classes = [
        "Língua Portuguesa", "Matemática", "Ciências", "Física", "Química", "Biologia",
        "Geografia", "História", "Inglês", "Filosofia"
    ]
    
}
