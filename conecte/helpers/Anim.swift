//
//  Anim.swift
//  conecte
//
//  Created by Adonio Silva on 25/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class Anim {
    
    class func rotate(v: UIView, durationInSeconds: CFTimeInterval, repeatCount: Float = .infinity) {
        let rotationAnimation = CABasicAnimation(keyPath: "transform.rotation")
        rotationAnimation.fromValue = 0
        rotationAnimation.toValue = Double.pi * 2
        rotationAnimation.duration = durationInSeconds
        rotationAnimation.repeatCount = repeatCount
        v.layer.add(rotationAnimation, forKey: nil)
    }
    
    class func fade(v: UIView?, targetAlpha: CGFloat, duration: TimeInterval=0.3, initialAlpha: CGFloat?=nil) {
        guard let v = v else { return }
        if let ini = initialAlpha { v.alpha = ini }
        UIView.animate(withDuration: duration) {
            v.alpha = targetAlpha
        }
    }
}
