import UIKit
import Alamofire

class App {
    
    private init() {}
    static var i: App = App();
    
    func isNetworking(_ option: Bool) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = option;
    }
    
    var version: String {
        get {
            return Bundle.main.infoDictionary?["CFBundleShortVersionString"] as? String ?? "1.2.3"
        }
    }
    
    var versionBuildNumbe: String {
        get {
            return Bundle.main.infoDictionary?["CFBundleVersion"] as? String ?? "0.0.0";
        }
    }
    
    func isNetworkAvailable() -> Bool {
        if let manager = NetworkReachabilityManager() {
            return manager.isReachable
        }
        return true
    }
    
    func openLink(_ surl: String) {
        if let url = URL(string :surl) {
            UIApplication.shared.open(url, options: [:]) { _ in
                // on completion
            }
        }
    }
    
    func openLink(tel: String) {
        openLink("tel://\(tel)")
    }
    
    func openLink(mail: String) {
        openLink("mailto:\(mail)")
    }
    
    
    func presentConfig() {
        let url = URL(string: UIApplication.openSettingsURLString)!
        UIApplication.shared.open(url, options: [:])
    }
    
    func hideKeyboard() {
        UIApplication.shared.sendAction(#selector(UIApplication.resignFirstResponder), to: nil, from: nil, for: nil)
    }

}
