//
//  ResultEx.swift
//  conecte
//
//  Created by Adonio Silva on 26/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

enum ResultEx<M> {
    
    case success(M)
    case failure(String)
    
}
