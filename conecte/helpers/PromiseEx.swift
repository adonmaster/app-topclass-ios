//
//  ResultEx.swift
//  conecte
//
//  Created by Adonio Silva on 25/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class PromiseEx<M> {
    
    typealias Resolver = (M)->()
    typealias Rejector = (String)->()
    typealias Doner = ()->()
    
    private var resolver: Resolver?
    private var rejector: Rejector?
    private var doner: Doner?
    
    private var wasResolved = false
    
    init(cb: @escaping (@escaping Resolver, @escaping Rejector)->()) {
        Task.main {
            cb(self.resolver ?? {_ in }, self.rejector ?? {_ in })
        }
    }
    
    @discardableResult func then(cb: @escaping Resolver) -> Self {
        resolver = { m in
            if !self.wasResolved {
                cb(m)
                self.doner?()
                self.wasResolved = true
            }
        }
        return self
    }
    
    @discardableResult func catchIt(cb: @escaping Rejector) -> Self {
        rejector = { reason in
            if !self.wasResolved {
                cb(reason)
                self.doner?()
                self.wasResolved = true
            }
        }
        return self
    }
    
    @discardableResult func done(cb: @escaping Doner) -> Self {
        self.doner = cb
        return self
    }
    
}
