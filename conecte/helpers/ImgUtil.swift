//
//  ImgUtil.swift
//  conecte
//
//  Created by Adonio Silva on 26/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import Kingfisher
import Photos

class ImgUtil {
    
    class func load(url: String?, _ img: UIImageView, placeholder: UIImage?, errorImage: UIImage?=nil) {
        guard let surl = url, !Str.isEmpty(surl), let u = URL(string: surl) else {
            img.image = placeholder
            return
        }
        
        let options: KingfisherOptionsInfo = [.fromMemoryCacheOrRefresh, .transition(.fade(0.3))]
        img.kf.setImage(with: u, placeholder: placeholder, options: options) { [weak img]  r in
            switch r {
            case .failure(_):
                img?.image = errorImage
            default:
                break
            }
        }
    }
    
    class func load(url: String?) -> PromiseEx<UIImage> {
        return PromiseEx { resolve, reject in
            guard let surl = url, !Str.isEmpty(surl), let u = URL(string: surl) else {
                reject("empty url")
                return
            }
            
            KingfisherManager.shared.retrieveImage(with: u, completionHandler: { r in
                switch r {
                case .success(let img):
                    resolve(img.image)
                case .failure(let err):
                    reject(err.localizedDescription)
                }
            })
        }
    }
    
    class func requestFromPicker(_ phasset: PHAsset?, size: Img.LimitType?=nil) -> PromiseEx<UIImage> {
        return PromiseEx { resolve, reject in
            
            guard let pha = phasset else {
                reject("Empty resource")
                return
            }
            
            var w = CGFloat(pha.pixelWidth)
            var h = CGFloat(pha.pixelHeight)
            let ratio = w/h
            if let sz = size {
                switch sz {
                case .width(let v):
                    w = w > v ? v : w
                    h = w / ratio
                case .height(let v):
                    h = h > v ? v : h
                    w = ratio * h
                }
            }
            
            let size = CGSize(width: w, height: h)
            let options = PHImageRequestOptions()
            options.isSynchronous = true
            Task.main {
                PHImageManager.default()
                    .requestImage(for: pha, targetSize: size, contentMode: .default, options: options) { img, dict in
                        if img == nil {
                            reject("Empty data")
                        } else {
                            resolve(img!)
                        }
                }
            }
            
        } // promise
    } // func
    
}
