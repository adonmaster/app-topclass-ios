import UIKit
import Foundation

class NotifyMessageView: UIView {

    // static
    
    class func f() -> NotifyMessageView {
        return Bundle.main.loadNibNamed("notify-message", owner: nil, options: nil)![0] as! NotifyMessageView
    }
    
    // ui
    
    @IBOutlet weak var uiLeftIcon: UILabel!
    @IBOutlet weak var uiTitle: UILabel!
    @IBOutlet weak var uiMessage: UILabel!
    
}
