import UIKit
import SwiftEntryKit

class Notify {
    
    private init() {}
    
    static var cLoading: String? {
        didSet {
            if let l = cLoading {
                cError = nil
                loading(msg: l)
            } else {
                unloading()
            }
        }
    }
    
    static var cError: String? {
        didSet {
            if let e = cError {
                error(msg: e)
            } else {
                unerror()
            }
        }
    }
    
    class func info(msg: String, title: String="Atenção", position: Position = .top, displayDuration: Double = 8.5)
    {
        var attr = instantiateAttrs(position: position, displayDuration: displayDuration)
        attr.name = "info"
        
        let v = instantiateView(msg: msg, title: title, bkndColor: Cl.mc.purple500Color())
        SwiftEntryKit.display(entry: v, using: attr)
    }
    
    class func error(msg: String, title: String="Atenção", position: Position = .top, displayDuration: Double = 8.5)
    {
        var attr = instantiateAttrs(position: position, displayDuration: displayDuration)
        attr.name = "error"
        
        let v = instantiateView(msg: msg, title: title, bkndColor: Cl.mc.red500Color())
        SwiftEntryKit.display(entry: v, using: attr)
    }
    
    class func unerror() {
        SwiftEntryKit.dismiss(.specific(entryName: "error"), with: nil)
    }
    
    class func loading(msg: String, title: String="Carregando...", position: Position = .top)
    {
        guard !SwiftEntryKit.isCurrentlyDisplaying(entryNamed: "loading") else {
            return
        }
        
        var attr = instantiateAttrs(position: position, displayDuration: .infinity)
        attr.entryInteraction = .forward
        attr.name = "loading"
        
        let v = instantiateView(msg: msg, title: title, bkndColor: Cl.mc.blue500Color())
        v.uiLeftIcon.text = ""
        attr.lifecycleEvents = .init(didAppear: {
            Anim.rotate(v: v.uiLeftIcon, durationInSeconds: 1.5)
        })

        SwiftEntryKit.display(entry: v, using: attr)
    }
    
    class func unloading() {
        SwiftEntryKit.dismiss(.specific(entryName: "loading"), with: nil)
    }
    
    // misc
    
    enum Position {
        case top
        case bottom
        case center
        
        func translateTo() -> EKAttributes.Position {
            switch self {
            case .bottom: return .bottom
            case .top: return .top
            case .center: return .center
            }
        }
    }
    
}

// MARK: utils
extension Notify {
    
    private class func instantiateView(msg: String, title: String, bkndColor: UIColor) -> NotifyMessageView {
        let v = NotifyMessageView.f()
        v.uiTitle.text = title
        v.uiMessage.text = msg
        v.backgroundColor = bkndColor
        return v
    }
    
    private class func instantiateAttrs(position: Position, displayDuration: Double) -> EKAttributes {
        var attr = EKAttributes.topFloat
        attr.windowLevel = .alerts
        attr.shadow = .active(with: .init(color: .black, opacity: 0.5, radius: 5, offset: .zero))
        attr.displayDuration = displayDuration
        attr.position = position.translateTo()
        return attr
    }
    
}
