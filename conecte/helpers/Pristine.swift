//
//  Pristine.swift
//  conecte
//
//  Created by Adonio Silva on 05/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation


class Pristine {
    
    static let shared = Pristine()
    private init() {}
    
    private var cache = [String:Int]()
    
    func itIsAndInc(key: String) -> Bool {
        let i = cache[key] ?? 0
        cache[key] = i + 1
        return i == 0
    }
    
    func itIs(key: String) -> Bool {
        return (cache[key] ?? 0) == 0
    }
    
}
