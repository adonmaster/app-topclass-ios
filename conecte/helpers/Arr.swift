import UIKit

class Arr<M> {
    
    private var list: [M]
    
    init(_ list: [M]) {
        self.list = list
    }
    
    func apply(_ cb: (inout [M])->()) -> Arr {
        cb(&list)
        return self
    }
    
    func categorize<K: Hashable>(_ cb: (M)->K) -> [K:[M]] {
        var r: [K:[M]] = [:]
        list.forEach { i in
            let key = cb(i)
            r[key] = r[key] ?? []
            r[key]!.append(i)
        }
        return r
    }
    
    func categorize<K: Hashable>(_ cb: (M)->K) -> [(K, [M])] {
        var r: [(K, [M])] = []
        list.forEach { i in
            let key = cb(i)
            if let index = r.firstIndex(where: { $0.0==key }) {
                r[index].1.append(i)
            } else {
                r.append((key, [i]))
            }
        }
        return r
    }

}
