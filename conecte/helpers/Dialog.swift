//
//  Dialog.swift
//  conecte
//
//  Created by Adonio Silva on 23/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class Dialog {
    
    private var title: String = "Atenção"
    private var msg: String = "Mensagem"
    private var hasOk = false
    private var hasCancel = false
    private var destructiveTitle: String? = nil
    
    private var actions: [String:(UIAlertAction)->Void] = [:]
    private var actionKeys: [String] = []
    
    weak private var sender: UIViewController?
    private var style: UIAlertController.Style;
    weak private var mPopoverSourceView: UIView?
    
    init(sender: UIViewController) {
        self.style = .alert;
        self.sender = sender;
    }
    
    func stylesheet(popoverSourceView: UIView?) -> Dialog {
        self.style = .actionSheet
        self.mPopoverSourceView = popoverSourceView
        return self
    }
    
    func title(_ title: String) -> Dialog {
        self.title = title
        return self
    }
    
    func msg(_ msg: String) -> Dialog {
        self.msg = msg;
        return self
    }
    
    func ok(_ option: Bool=true) -> Dialog {
        self.hasOk = option
        return self
    }
    
    func destructive(_ title: String) -> Dialog {
        self.destructiveTitle = title
        return self
    }
    
    func cancel(_ option: Bool=true) -> Dialog {
        self.hasCancel = option
        return self
    }
    
    @discardableResult func action(key: String, cb: @escaping (UIAlertAction)->Void) -> Dialog {
        actions[key] = cb
        actionKeys.append(key)
        return self
    }
    
    func present(onOk: (()->Void)?=nil, onCancel: (()->Void)?=nil, onDestructive: (()->Void)?=nil, _ completion:(()->Void)?=nil)
    {
        let d = UIAlertController(title: title, message: msg, preferredStyle: style)
        
        // custom actions
        actionKeys.forEach { key in
            d.addAction(UIAlertAction(title: key, style: .default, handler: actions[key]))
        }
        
        if (hasOk) {
            d.addAction(UIAlertAction(title: "Ok", style: .default, handler: { a in
                onOk?()
            }))
        }
        
        if let desTitle = destructiveTitle {
            d.addAction(UIAlertAction(title: desTitle, style: .destructive, handler: { a in
                onDestructive?()
            }))
        }
        
        if (hasCancel) {
            d.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { a in
                onCancel?()
            }))
        }
        
        d.popoverPresentationController?.sourceView = mPopoverSourceView
        
        // present
        sender?.present(d, animated: true, completion: completion)
    }
    
}

