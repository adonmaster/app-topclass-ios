import UIKit

class Cl {
    
    static let mc = GMColor.self
    
    static let white = UIColor.white
    
    static func quad(_ def: UIColor, _ list: [UIColor:Bool]) -> UIColor {
        var r = def
        list.forEach { cl, b in
            if b {
                r = cl
                return
            }
        }
        return r
    }
    
}
