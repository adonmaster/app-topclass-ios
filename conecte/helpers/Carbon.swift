import Foundation


class Carbon {
    
    // static
    
    static var locale: Locale {
        return Locale(identifier: Locale.preferredLanguages.first ?? "pt_BR")
    }
    
    class func parse(_ s: String, _ format: String="dd/MM/yyyy HH:mm:ss") -> Carbon? {
        let fr = DateFormatter()
        fr.locale = Locale(identifier: "en_US_POSIX")
        fr.timeZone = TimeZone.current
        fr.dateFormat = format
        
        guard let d = fr.date(from: s) else { return nil }
        
        return Carbon(d)
    }
    
    class func parseFromSqlDate(_ s: String) -> Carbon? {
        return parse(s, "yyyy-MM-dd")
    }
    
    class func parseFromJson(_ s: String) -> Carbon? {
        return parse(s, "yyyy-MM-dd HH:mm:ss")
    }
    
    class func parseFromJsonT(_ s: String) -> Carbon? {
        return parse(s, "yyyy-MM-dd'T'HH:mm:ss")
    }
    
    class func parseBr(_ s: String) -> Carbon? {
        return parse(s, "dd/MM/yyyy HH:mm:ss")
    }
    
    // instance
    
    private var dt: Date
    private let calendar: Calendar
    private lazy var formatter: DateFormatter = {
        let dt = DateFormatter()
        dt.locale = Carbon.locale
        dt.timeZone = TimeZone.current
        return dt
    }()
    
    private var now: Date { return Date(timeIntervalSinceNow: 0) }
    
    init(_ dt: Date?=nil, zeroHours: Bool=false) {
        self.dt = dt ?? Date(timeIntervalSinceNow: 0)
        calendar = Calendar.current
        
        if zeroHours {
            self.dt = calendar.date(bySettingHour: 0, minute: 0, second: 0, of: self.dt)!
        }
    }
    
    init(_ c: Carbon) {
        self.dt = c.get()
        calendar = Calendar.current
    }

    func clone() -> Carbon {
        return Carbon(dt)
    }
    
    @discardableResult func set(_ d: Date) -> Carbon {
        return Carbon(d)
    }
    
    func set(hours: Int, mins: Int, secs: Int=0) -> Carbon {
        let dd = calendar.date(bySettingHour: hours, minute: mins, second: secs, of: self.dt)!
        return Carbon(dd)
    }
    
    @discardableResult func add(_ value: Int, _ unit: Calendar.Component) -> Carbon {
        let dd = calendar.date(byAdding: unit, value: value, to: dt)!
        return Carbon(dd)
    }
    
    func startOfMonth() -> Carbon {
        return add(1 - getComponent(.day), .day)
    }
    
    func endOfMonth() -> Carbon {
        return startOfMonth().add(1, .month).add(-1, .day)
    }
    
    func get() -> Date {
        return dt
    }
    
    func getComponent(_ component: Calendar.Component) -> Int {
        return calendar.component(component, from: dt)
    }
    
    func getDate(pattern: String = "dd") -> String {
        return format(pattern: pattern)
    }
    
    func getDayOfWeek(pattern: String = "E") -> String {
        return format(pattern: pattern)
    }
    
    func diffIn(_ unit: Calendar.Component, from date: Date) -> Int {
        let start = calendar.ordinality(of: unit, in: .era, for: date)!
        let end = calendar.ordinality(of: unit, in: .era, for: dt)!
        return end - start
    }
    
    func diffFromNow(_ unit: Calendar.Component) -> Int {
        return diffIn(unit, from: now)
    }
    
    func betweenInDays(ini: Carbon, end: Carbon) -> Bool {
        let sini = ini.formatSqlDate()
        let send = end.formatSqlDate()
        let s = formatSqlDate()
        return s >= sini && s <= send
    }
    
    func isFuture(_ fromDate: Date?=nil) -> Bool {
        return dt > fromDate ?? now
    }
    
    func isToday() -> Bool {
        let pattern = "dd-MM-yyyy"
        return Carbon(dt).format(pattern: pattern) == Carbon().format(pattern: pattern)
    }
    
    func isTodaysMonth() -> Bool {
        let pattern = "MM-yyyy"
        return Carbon(dt).format(pattern: pattern) == Carbon().format(pattern: pattern)
    }
    
    func isSameDate(_ date: Date) -> Bool {
        return formatSqlDate() == Carbon(date).formatSqlDate()
    }
    
    func inBrRange(dayOfWeek: String, shift: String) -> Bool {
        let dow = ["dom": 1,"seg": 2, "ter": 3, "qua": 4, "qui": 5, "sex": 6, "sab": 7]
        if let dowInt = dow[String(dayOfWeek.prefix(3)).lowercased()] {
            if dowInt == getComponent(.weekday)
                && turno().lowercased() == shift.lowercased()
            {
                return true
            }
        }
        return false
    }
    
    func turno() -> String {
        let hour = getComponent(.hour)
        switch hour {
        case 5...11: return "Matutino"
        case 12...18: return "Vespertino"
        default: return "Noturno"
        }
    }
    
    //
    
    func format(pattern: String="dd-MM-yyyy") -> String {
        formatter.dateFormat = pattern
        
        return formatter.string(from: dt)
    }
    
    func formatBrDate() -> String {
        return format(pattern: "dd/MM/yyyy")
    }
    
    func formatBrDateTime() -> String {
        return format(pattern: "dd/MM/yyyy HH:mm:ss")
    }
    
    func formatBrDateTimeMin() -> String {
        return format(pattern: "dd/MM/yyyy HH:mm")
    }
    
    func formatSqlDate() -> String {
        return format(pattern: "yyyy-MM-dd")
    }
    
    func formatSqlMonth() -> String {
        return format(pattern: "yyyy-MM")
    }
    
    func formatSql() -> String {
        return format(pattern: "yyyy-MM-dd HH:mm:ss")
    }
    
    func formatJsonT() -> String {
        return format(pattern: "yyyy-MM-dd'T'HH:mm:ss")
    }
    
}

// MARK: time ago

extension Carbon {
    
    func diffForHuman() -> String
    {
        let now = Date()
        let diff = calendar.dateComponents([.year, .month, .day, .hour, .minute, .second], from: now, to: dt)
        
        let cbP: (Int,String,String,String,String)->String? = { i, sp, sn, mp, mn in
            if i == 0 { return nil }
            if i == 1 { return sp }
            if i == -1 { return sn }
            if i > 1 { return mp.replacingOccurrences(of: "#n", with: String(abs(i))) }
            if i < 1 { return mn.replacingOccurrences(of: "#n", with: String(abs(i))) }
            return nil
        }
        
        if let s = cbP(diff.year!, "em um ano", "um ano atrás", "em #n anos", "#n anos atrás") {
            return s
        }
        
        if let s = cbP(diff.month!, "em um mês", "mês passado", "em #n meses", "#n meses atrás") {
            return s
        }
        
        if let s = cbP(diff.day!, "em um dia", "ontem", "em #n dias", "#n dias atrás") {
            return s
        }
        
        if let s = cbP(diff.hour!, "em uma hora", "uma hora atrás", "em #n horas", "#n horas atrás") {
            return s
        }
        
        if let s = cbP(diff.minute!, "em um minuto", "um minuto atrás", "em #n minutos", "há #n minutos") {
            return s
        }
        
        if let s = cbP(diff.second!, "em um segundo", "um segundo atrás", "em alguns segundos", "segundos atrás") {
            return s
        }
        
        return "agorinha"
    }
    
}

// MARK: comparable
extension Carbon: Comparable {
    
    static func > (lhs: Carbon, rhs: Carbon) -> Bool {
        return lhs.get() > rhs.get()
    }

    static func < (lhs: Carbon, rhs: Carbon) -> Bool {
        return lhs.get() < rhs.get()
    }
    
    static func == (lhs: Carbon, rhs: Carbon) -> Bool {
        return lhs.get() == rhs.get()
    }

    
}
