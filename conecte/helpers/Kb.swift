//
//  Kb.swift
//  conecte
//
//  Created by Adonio Silva on 25/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class Kb {
    
    class func setInputAccessoryButton(textField: AnyObject, title: String, target: AnyObject?, action: Selector)
    {
        // create tool bar
        let toolbar = UIToolbar.init(frame: CGRect(x: 0, y: 0, width: 320, height: 50));
        
        // flexible space
        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil);
        
        // button
        let done = UIBarButtonItem(title: title, style: UIBarButtonItem.Style.plain, target: target, action: action);
        
        // config
        toolbar.items = [flexSpace, done];
        toolbar.sizeToFit();
        
        // do the thing
        if textField is UITextField {
            (textField as! UITextField).inputAccessoryView = toolbar;
        }
        else if textField is UITextView {
            (textField as! UITextView).inputAccessoryView = toolbar;
        }
    }
    
}
