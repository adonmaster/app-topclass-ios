//
//  Num.swift
//  conecte
//
//  Created by Adonio Silva on 06/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class Num {
    
    static func inRange(v: Double, min: Double, max: Double) -> Double {
        if v > max { return max }
        if v < min { return min }
        return v
    }
    
    static func inRange(v: Float, min: Float, max: Float) -> Float {
        if v > max { return max }
        if v < min { return min }
        return v
    }
    
    static func inRange(v: Int, min: Int, max: Int) -> Int {
        if v > max { return max }
        if v < min { return min }
        return v
    }

    static func fromBrCurrency(_ v: String?, def: Double) -> Double {
        if let v = v {
            let stripped = v.replacingOccurrences(of: "R$", with: "")
                .replacingOccurrences(of: ".", with: "")
                .replacingOccurrences(of: ",", with: ".")
                .trimmed
            return Double(stripped) ?? def
        }
        return def
    }
    
}
