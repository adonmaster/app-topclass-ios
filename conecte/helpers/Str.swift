import Foundation
import UIKit

class Str {
    
    class func mergeUriPart(_ base: String, _ uri: String) -> String {
        let a: String = base.hasSuffix("/") ? String(base.dropLast()) : base;
        let b: String = uri.hasPrefix("/") ? String(uri.dropFirst()) : uri;
        return a + "/" + b;
    }
    
    class func plural<T>(_ n: Int, _ zero: T, _ one: T, _ many: (Int)->T) -> T {
        if n <= 0 { return zero }
        if n == 1 { return one }
        return many(n)
    }
    
    class func capitalizeFirst(s: String) -> String {
        return s.prefix(1).uppercased() + s.lowercased().dropFirst()
    }
    
    class func randomAlphaNumeric(size: Int=16) -> String {
        let letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        return String((0..<size).map { _ in letters.randomElement()! })
    }
    
    class func isEmpty(_ s: String?) -> Bool {
        return s?.trimmed.isEmpty ?? true
    }
    
    class func split(_ s: String, _ separator: Character) -> [String] {
        let list = s.split(separator: separator)
        return list.map { String($0) }
    }
    
    class func coalesce(_ list: String?...) -> String {
        for s in list {
            if s?.trimmed.isEmpty == false { return s! }
        }
        return ""
    }
    
    class func colorize(_ s: String, _ colors: [UIColor: Bool]) -> NSAttributedString {
        let span = Span(s)
        colors.forEach { (color, b) in
            if (b) { span.color(color) }
        }
        return span.get()
    }
    
    class func colorizeEmpty(_ s: String?, emptyText: String, _ emptyColor: UIColor) -> NSAttributedString {
        let isEmpty = s?.isEmpty ?? true
        return colorize(isEmpty ? emptyText : s!, [emptyColor: (s ?? "").trimmed.isEmpty])
    }
    
    class func colorizeEmpty(_ s: String?, emptyText: String) -> NSAttributedString {
        return colorizeEmpty(s, emptyText: emptyText, Cl.mc.grey500Color())
    }
    
    
    class func number(_ d: NSNumber?, digits: Int=2, prefix: String="", suffix: String="", def: String="-") -> String {
        guard let d = d else { return def }
        
        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.maximumFractionDigits = digits
        formatter.minimumFractionDigits = digits
        return prefix + formatter.string(from: d as NSNumber)! + suffix
    }
    
    class func number(_ f: Float?, digits: Int=2, prefix: String="", suffix: String="", def: String="-") -> String {
        return number(f as NSNumber?, digits: digits, prefix: prefix, suffix: suffix, def: def)
    }
    
    class func number(_ d: Double?, digits: Int=2, prefix: String="", suffix: String="", def: String="-") -> String {
        return number(d as NSNumber?, digits: digits, prefix: prefix, suffix: suffix, def: def)
    }
    
    class func obfuscateCardNo(_ s: String?) -> String {
        let last = s?.split(separator: " ").map({ String($0) }).last ?? "0000"
        return "xxxx xxxx xxxx " + last
    }
    
    class func obfuscateCardCvv(_ s: String?) -> String {
        return "xxx"
    }
    
    class func count(_ s: String?) -> Int {
        return s?.count ?? 0
    }
    
}
