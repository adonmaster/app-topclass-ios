//
//  ContractRModel.swift
//  conecte
//
//  Created by Adonio Silva on 17/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class ContractRModel: Decodable {
    
    var payload: [Item]
    
    class Item: Decodable {
        
        var id: Int
        var price: Double
        var body: BodyV2
        var client_user: Usr
        var pro_user: Usr
        var was_executed: Bool
        var due_at: String
        var updated_at: String
        var created_at: String
        
        //
        lazy var p = ContractItemPresenter(self)
        
        //
        class Usr: Decodable {
            var id: Int
            var name: String
            var email: String
            var avatar_thumb: String?
        }
        
        class BodyV2: Decodable {
            var version: Int
            var address_line: String
            var address_comp: String?
            var address_lat: Double
            var address_lng: Double
            var items: [BodyV2Item]
        }
        
        class BodyV2Item: Decodable {
            var dt: String
            var materia: String
            var duration: Int
            
            lazy var dtObject = Carbon.parseFromJson(dt)!
        }
        
    }
    
}
