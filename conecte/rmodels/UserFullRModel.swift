//
//  UserFullRModel.swift
//  conecte
//
//  Created by Adonio Silva on 11/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class UserFullRModel: Decodable {
    
    var id: Int
    var name: String
    var email: String
    var avatar_thumb: String?
    var avatar: String?
    var last_active: String?
    var is_pro: Bool
    var skills: [Skill]
    
    lazy var p = UserFullPresenter(self)
    
    
    class Skill: Decodable {
        
        var id: Int
        var desc: String
        var pivot: SkillPivot
        
    }
    
    class SkillPivot: Decodable {
        var comp: String?
        var option: Int
    }
    
}
