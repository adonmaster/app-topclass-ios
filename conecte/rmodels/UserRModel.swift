//
//  UserRModel.swift
//  conecte
//
//  Created by Adonio Silva on 06/09/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation


class UserRModel: Decodable {
    
    var payload: Payload
    
    class Payload: Decodable {
        var user: Usr
    }
    class Usr: Decodable {
        var id: Int
        var name: String
        var email: String
        var avatar_thumb: String?
        var is_pro: Bool
    }
}
