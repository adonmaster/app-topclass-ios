//
//  ProposalCardTokenRModel.swift
//  conecte
//
//  Created by Adonio Silva on 14/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation


struct ProposalCardTokenRModel: Decodable {
    
    var payload: Payload
    
    //
    struct Payload: Decodable {
        var token: String
    }
    
}
