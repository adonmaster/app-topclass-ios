//
//  TransactionRModel.swift
//  conecte
//
//  Created by Adonio Silva on 31/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class TransactionRModel: Decodable {
    
    var payload: Pagination
    
    class Pagination: Decodable {
        var data: [Item]
    }
    
    class Item: Decodable {
        var id: Int
        var operation_type: String
        var amount: Double
        var balance: Double
        var desc: String
        var created_at: String
        
        lazy var p = TransactionPresenter(self)
    }
    
}
