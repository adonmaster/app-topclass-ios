//
//  LoginCodeRModel.swift
//  conecte
//
//  Created by Adonio Silva on 26/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation


struct LoginCodeRModel: Decodable {

    var payload: Payload
    
    struct Payload: Decodable {
        var id: Int
        var api_token: String
        var avatar: String?
        var avatar_thumb: String?
        var is_pro: Bool
    }
}
