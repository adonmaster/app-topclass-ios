//
//  ProposalRModel.swift
//  conecte
//
//  Created by Adonio Silva on 16/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation


class ProposalRModel: Decodable {
    
    var payload: [ProposalItemRModel]
    
}
