//
//  AdRModel.swift
//  conecte
//
//  Created by Adonio Silva on 27/08/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class AdRModel: Decodable {
    
    var payload: [Item]
    
    class Item: Decodable {
        var id: Int
        var client_user_id: Int
        var body: BodyV2
        var client_user: Usr
        var pros: [Pr]
        var created_at: String
        
        lazy var p = AdItemPresenter(self)
    }
    
    //
    class Usr: Decodable {
        var id: Int
        var name: String
        var email: String
        var avatar_thumb: String?
    }
    
    class Pr: Decodable {
        var id: Int
        var price: Double
        var pro_user: Usr
    }
    
    class BodyV2: Decodable {
        var version: Int
        var address_line: String
        var address_comp: String?
        var address_lat: Double
        var address_lng: Double
        var items: [BodyV2Item]
    }
    
    class BodyV2Item: Decodable {
        var dt: String
        var materia: String
        var duration: Int
        
        lazy var dtObject = Carbon.parseFromJson(dt)!
    }
    
    
}
