//
//  AppDelegate.swift
//  conecte
//
//  Created by Adonio Silva on 18/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit
import CoreData


@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    static var shared: AppDelegate {
        get {
            UIApplication.shared.delegate as! AppDelegate
        }
    }
    private var apns_loading = false

    //
    
    func test() {
        
        let db = Repo.pref.get("app@ver", "nothing")
        if db != App.i.version {
            Repo.pref.set("app@ver", App.i.version)
            
            Repo.user._deleteAll()
        }

    }
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        
        // test()
        Session.shared.loadActive()
        
        //
        configApns()
        
        return true
    }

    func applicationWillTerminate(_ application: UIApplication) {
        self.saveContext()
    }

    // MARK: - Core Data stack

    lazy var persistentContainer: NSPersistentContainer = {
        let container = NSPersistentContainer(name: "conecte")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()

    // MARK: - Core Data Saving support

    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
}

// MARK: apns
extension AppDelegate {
    
    func configApns() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .badge, .sound]) { granted, err in
            if Session.shared.user != nil && Session.shared.shouldConfigApns() {
                Task.main {
                    UIApplication.shared.registerForRemoteNotifications()
                }
            }
        }
    }
    
    struct FormGcm: Encodable {
        var gcm: String
        var token: String
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        guard let user = Session.shared.user else { return }
        if deviceToken.count > 0
        {
            let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
            
            if Repo.pref.get("apns@\(user.remote_id)") == token { return }
            if apns_loading { return }
            
            apns_loading = true
            
            let params = FormGcm(gcm: "apn", token: token)
            Http.shared.post(BaseRModel.self, uri: "gcm", params)
                .then { _ in
                    
                    Repo.pref.set("apns@\(user.remote_id)", token)
                    
                }
                .catchIt { reason in
                    print(reason)
                }
                .done {
                    self.apns_loading = false
                }
        }
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error.localizedDescription)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void)
    {
        if let aps = userInfo["aps"] as? [String:Any],
            let alert = aps["alert"] as? [String:String],
            let title = alert["title"], let body = alert["body"]
        {
            Notify.info(msg: body, title: title)
        }
        
        completionHandler(.noData)
    }
    
}


