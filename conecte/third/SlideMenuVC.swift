import UIKit

class SlideMenuVC: SlideMenuController {
    
    @IBInspectable var uiStoryIdMain: String!
    @IBInspectable var uiStoryIdLeft: String? = nil
    @IBInspectable var uiStoryIdRight: String? = nil
    
    func mainStoryboardToLoad() -> String? {
        return nil
    }
    
    private func initLocal()
    {
        if let s = storyboard
        {
            if let sb = mainStoryboardToLoad(), sb != "Main" {
                self.mainViewController = UIStoryboard(name: sb, bundle: nil).instantiateInitialViewController()
            } else {
                self.mainViewController = s.instantiateViewController(withIdentifier: uiStoryIdMain!)
            }
            
            if let right = uiStoryIdRight {
                rightViewController = s.instantiateViewController(withIdentifier: right)
            }
            
            if let left = uiStoryIdLeft {
                leftViewController = s.instantiateViewController(withIdentifier: left)
            }
        }
    }
    
    override func awakeFromNib() {
        
        initLocal()
        
        super.awakeFromNib()
    }
    
}
