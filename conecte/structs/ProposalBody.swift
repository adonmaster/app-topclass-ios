//
//  ProposalBody.swift
//  conecte
//
//  Created by Adonio Silva on 14/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class ProposalBody {
    
    struct Body: Encodable {
        var version: Int
        var items: [Item]
        var address_line: String
        var address_comp: String?
        var address_lat: Double
        var address_lng: Double
    }
    
    struct Item: Encodable {
        var dt: String
        var materia: String
        var duration: Int
    }
    
    class func toJson(
        dates: [ProposalMultiDialogVC.Item], address: String, addressComp: String?, coord: (Double, Double)
    ) -> Body
    {
        return Body(
            version: 2, items: buildItems(dates), address_line: address, address_comp: addressComp,
            address_lat: coord.0, address_lng: coord.1
        )
    }
    
    class private func buildItems(_ dts: [ProposalMultiDialogVC.Item]) -> [Item] {
        return dts.map {
            Item(dt: Carbon($0.dt).formatSql(), materia: $0.materia, duration: $0.duration)
        }
    }
    
}
