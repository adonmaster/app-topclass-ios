//
//  Session.swift
//  conecte
//
//  Created by Adonio Silva on 25/06/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

class Session {
    
    //
    static let shared = Session()
    
    //
    private init() {}
    
    //
    var user: User? = nil
    
    
    //
    func saveUser(remote_id: Int, name: String, email: String, avatar: String?, token: String, isPro: Bool) {
        let usr = Repo.user.save(remote_id: remote_id, name: name, email: email, avatar: avatar, token: token, isPro: isPro)
        
        Repo.user.activateUser(email: email)
        
        self.user = usr
    }
    
    func updateUser(email: String, name: String, avatar: String?) {
        self.user = Repo.user.updateUser(email: email, name: name, avatar: avatar)
    }
    
    
    func loadActive() {
        self.user = Repo.user.firstActive()
    }

    
    private var mShouldConfigApns = true
    func shouldConfigApns() -> Bool {
        if mShouldConfigApns || (Int.random(in: 0...9) < 2) {
            mShouldConfigApns = false
            return true
        }
        return false
    }
    
}
