import UIKit;

class ImageViewRounded: UIImageView {

    override func awakeFromNib()
    {
        super.awakeFromNib()
        
        roundIt()
    }
    
    func roundIt()
    {
        layer.cornerRadius = frame.height/2;
        clipsToBounds = true;
    }
    
}
