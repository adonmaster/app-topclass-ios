//
//  ViewEx.swift
//  conecte
//
//  Created by Adonio Silva on 14/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class ViewEx: UIView {
    
    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            if shadowOpacity <= 0 {
                layer.masksToBounds = true
            }
        }
    }
    
}
