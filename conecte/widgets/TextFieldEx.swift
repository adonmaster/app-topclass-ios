import UIKit;

@IBDesignable class TextFieldEx: UITextField {
    
    @IBInspectable var accessoryText: String?
    @IBInspectable var accessoryColor: UIColor?
    
    @IBInspectable var insetTop: CGFloat = 10;
    @IBInspectable var insetRight: CGFloat = 10;
    @IBInspectable var insetBottom: CGFloat = 10;
    @IBInspectable var insetLeft: CGFloat = 10;
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupMask()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupMask()
    }
    
    fileprivate func insetMaker(_ bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets.init(top: insetTop, left: insetLeft, bottom: insetBottom, right: insetRight))
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return insetMaker(bounds)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return insetMaker(bounds)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return insetMaker(bounds)
    }
    
    override func awakeFromNib() {
        // accessory
        if let text = self.accessoryText, !text.isEmpty {
            Kb.setInputAccessoryButton(textField: self, title: text, target: self, action: #selector(onAccessoryDoneClicked))
        }
    }
    
    @objc func onAccessoryDoneClicked() {
        resignFirstResponder();
    }
    
    private func setupMask() {
        //
    }
}
