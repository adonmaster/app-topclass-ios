//
//  TextFieldMaskEx.swift
//  conecte
//
//  Created by Adonio Silva on 13/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import UIKit

class TextFieldMaskEx: JMMaskTextField {
    
    @IBInspectable var insetTop: CGFloat = 10;
    @IBInspectable var insetRight: CGFloat = 10;
    @IBInspectable var insetBottom: CGFloat = 10;
    @IBInspectable var insetLeft: CGFloat = 10;
    
    fileprivate func insetMaker(_ bounds: CGRect) -> CGRect {
        return bounds.inset(by: UIEdgeInsets.init(top: insetTop, left: insetLeft, bottom: insetBottom, right: insetRight))
    }
    
    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return insetMaker(bounds)
    }
    
    override func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return insetMaker(bounds)
    }
    
    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return insetMaker(bounds)
    }
    
}
