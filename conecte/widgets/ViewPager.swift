import UIKit

class ViewPager: UIView {
    
    // static helpers
    static var animatorCrossFade: ViewPagerSwapAnimator {
        get {
            return CrossDissolveViewPagerAnimator()
        }
    }
    
    // ui & state
    
    @IBOutlet var rootController: UIViewController!
    @IBInspectable var controllers: String? = nil
    
    @IBOutlet var delegate: UIViewController?
    private var mDelegate: ViewPagerDelegate? {
        get {
            return delegate as? ViewPagerDelegate
        }
    }
    
    private var controllerList: [String] {
        get {
            if let s = controllers {
                return s.split(separator: " ").map { String($0) }
            }
            return []
        }
    }
    
    private weak var currentController: UIViewController?
    
    private var cache = [String: UIViewController]()

    
    // methods
    
    override func awakeFromNib() {
        super.awakeFromNib()

        guard let storyboardId = controllerList.first else { return }
        swap(storyboardId: storyboardId, animator: nil)
    }
    
    @discardableResult func swap(storyboardId: String, animator: ViewPagerSwapAnimator?) -> UIViewController?
    {
        let vc = newvc(storyboardId)
        
        prepare(vc)
        
        vc.view.translatesAutoresizingMaskIntoConstraints = false
        cycle(from: currentController, to: vc, animator)
        
        return vc
    }
    
    private func prepare(_ vc: UIViewController) {
        mDelegate?.viewPager(onPrepare: vc)
        (vc as? ViewPagerItemDelegate)?.viewPagerOnItemPrepare?()
    }
    
    @discardableResult func swap(controllerIndex: Int, animator: ViewPagerSwapAnimator?) -> UIViewController? {
        return swap(storyboardId: controllerList[controllerIndex], animator: animator)
    }
    
    private func cycle(from a: UIViewController?, to b: UIViewController, _ animator: ViewPagerSwapAnimator?)
    {
        a?.willMove(toParent: nil)

        rootController.addChild(b)
        addsubview(b.view, toView: self)
        
        a?.view.layoutIfNeeded()
        b.view.layoutIfNeeded()
        
        // defer?
        let deferrer: (UIViewController?, UIViewController)->Void = { a, b in
            a?.view.removeFromSuperview()
            a?.removeFromParent()
            
            b.didMove(toParent: self.rootController)
            self.currentController = b
        }
        
        //
        if let a = a, let animator = animator {
            animator.viewPagerAnimator(rootController, from: a, to: b) { _ in
                deferrer(a, b)
            }
        }
        // theres no old vc
        else {
            deferrer(a, b)
        }
    }
    
    
}

// MARK: protocols
protocol ViewPagerDelegate {
    func viewPager(onPrepare vc: UIViewController)
}

protocol ViewPagerSwapAnimator {
    func viewPagerAnimator(_ vc: UIViewController, from a: UIViewController?, to b: UIViewController,  onDone: @escaping (Bool)->())
}

@objc protocol ViewPagerItemDelegate {
    @objc optional func viewPagerOnItemPrepare()
    @objc optional func viewPagerOnBoot()
}

// MARK: utils
extension ViewPager {
    
    private func newvc(_ storyboardId: String) -> UIViewController {
        var vc = cache[storyboardId]

        if vc == nil {
            vc = rootController.storyboard!.instantiateViewController(withIdentifier: storyboardId)
            (vc as? ViewPagerItemDelegate)?.viewPagerOnBoot?()
            cache[storyboardId] = vc
        }
        
        return vc!
    }
    
    private func addsubview(_ v: UIView, toView parent: UIView) {
        parent.addSubview(v)
        
        let viewBindingDict: [String:Any] = ["subview": v]
        parent.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "H:|[subview]|", options: [], metrics: nil, views: viewBindingDict)
        )
        parent.addConstraints(
            NSLayoutConstraint.constraints(withVisualFormat: "V:|[subview]|", options: [], metrics: nil, views: viewBindingDict)
        )
    }
    
}
