import UIKit

class CrossDissolveViewPagerAnimator: ViewPagerSwapAnimator {

    func viewPagerAnimator(_ vc: UIViewController, from a: UIViewController?, to b: UIViewController, onDone: @escaping (Bool) -> ())
    {
        b.view.alpha = 0
        b.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.4, animations: {
            b.view.alpha = 1
            a?.view.alpha = 0
        }, completion: onDone)
    }
    
}
