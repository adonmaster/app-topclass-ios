import UIKit

@IBDesignable extension UIView {
    
    @IBInspectable var isVisible: Bool {
        get {
            return !isHidden
        }
        set {
            isHidden = !newValue
        }
    }
    
    // shadow
    
    @IBInspectable var shadowOpacity: Float {
        get {
            return layer.shadowOpacity
        }
        set {
            layer.shadowOpacity = newValue
        }
    }
    
    @IBInspectable var shadowColor: UIColor? {
        get {
            guard let cl = layer.shadowColor else { return nil }
            return  UIColor(cgColor: cl)
        }
        set {
            layer.shadowColor = newValue?.cgColor
        }
    }
    
    @IBInspectable var shadowOffsetX: CGFloat {
        get {
            return layer.shadowOffset.width
        }
        set {
            layer.shadowOffset = CGSize(width: newValue, height: shadowOffsetY)
        }
    }
    
    @IBInspectable var shadowOffsetY: CGFloat {
        get {
            return layer.shadowOffset.height
        }
        set {
            layer.shadowOffset = CGSize(width: shadowOffsetX, height: newValue)
        }
    }
    
    var shadowRadius: CGFloat {
        get {
            return layer.shadowRadius
        }
        set {
            layer.shadowRadius = newValue
        }
    }
  /**
    // border
    @IBInspectable var borderWidth: CGFloat {
        set {
            layer.borderWidth = newValue
        }
        get {
            return layer.borderWidth
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        set {
            layer.borderColor = newValue?.cgColor
        }
        get {
            guard let cl = layer.borderColor else { return nil }
            return UIColor(cgColor: cl)
        }
    }

    // corner

    @IBInspectable var cornerRadius: CGFloat {
        get {
            return self.layer.cornerRadius
        }
        set {
            self.layer.cornerRadius = newValue
            if shadowOpacity <= 0 {
                layer.masksToBounds = true
            }
        }
    }
    */
    // visible
    var isVisibleAlphaAnim: Bool {
        get {
            return alpha == 1
        }
        set {
            let newAlpha: CGFloat = newValue ? 1 : 0
            UIView.animate(withDuration: 0.3) {
                self.alpha = newAlpha
            }
        }
    }
    
}
