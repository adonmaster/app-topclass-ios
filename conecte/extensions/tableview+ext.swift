import UIKit

@IBDesignable extension UITableView {
    
    @IBInspectable var refresherTint: UIColor? {
        get {
            return findRefresher()?.tintColor
        }
        set {
            findRefresher()?.tintColor = newValue
        }
    }
    
    func initRefresher(caller: UIViewController, action: Selector) {
        guard findRefresher()==nil else { return }
        
        let rc = UIRefreshControl()
        rc.addTarget(caller, action: action, for: .valueChanged)
        refreshControl = rc
    }
    
    private func findRefresher() -> UIRefreshControl? {
        return subviews.first(where: { $0 is UIRefreshControl }) as? UIRefreshControl
    }
    
    var isRefreshAnimating: Bool {
        get {
            return findRefresher()?.isRefreshing ?? false
        }
        set {
            refresherAnimate(option: newValue)
        }
    }
    
    private func refresherAnimate(option: Bool) {
        guard let refresher = findRefresher() else { return }

        if option {
            if let father = refresher.superview as? UIScrollView {
                father.setContentOffset(CGPoint(x: 0, y: father.contentOffset.y - frame.height), animated: true)
            }
            refresher.beginRefreshing()
        }
        
        else {
            refresher.endRefreshing()
        }
    }
    
}

