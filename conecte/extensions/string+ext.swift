import Foundation

extension String {
    
    func hasPrefixes(_ list: String?...) -> Bool {
        for s in list.compactMap({ $0 }) {
            if hasPrefix(s) { return true }
        }
        return false
    }
    
    var domainSegments: [String] {
        get {
            return split(separator: ".").map { c in String(c) }
        }
    }

    func stripped(from setList: CharacterSet...) -> String {
        var s = self
        for set in setList {
            s = String(String.UnicodeScalarView(s.unicodeScalars.filter({ !set.contains($0) })))
        }
        return s
    }
    
    func strippedFromPhoneSeparators() -> String {
        return stripped(from: .whitespacesAndNewlines, .init(charactersIn: "-.()"))
    }
    
    var trimmed: String {
        return trimmingCharacters(in: .whitespacesAndNewlines)
    }
    
    var trimmedRight: String {
        var t = self
        while t.hasSuffix(" ") || t.hasSuffix("\n") || t.hasSuffix("\r") {
            t = String(t.dropLast())
        }
        return t
    }
    
    func containsInsensitive(_ str: String?) -> Bool {
        guard let s = str else { return false }
        return range(of: s, options: .caseInsensitive) != nil
    }
    
    func formal1() -> String {
        return prefix(1).capitalized + lowercased().dropFirst()
    }
    
    func formal2() -> String {
        return capitalized
    }
    
    func formal3() -> String {
        return formal1().replacingOccurrences(of: "_", with: " ")
    }
    
    func def(_ s: String) -> String {
        if trimmed.isEmpty { return s }
        return self
    }
    
}
