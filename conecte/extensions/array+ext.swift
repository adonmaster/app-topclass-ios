//
//  array+ext.swift
//  conecte
//
//  Created by Adonio Silva on 08/07/20.
//  Copyright © 2020 Adonio Silva. All rights reserved.
//

import Foundation

extension Array where Element: Hashable {
    
    func uniqued() -> [Element] {
        var seen = Set<Element>()
        return filter { seen.insert($0).inserted }
    }
    
    func get(_ index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
    
}
